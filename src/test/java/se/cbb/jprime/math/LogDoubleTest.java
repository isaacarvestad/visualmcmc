package se.cbb.jprime.math;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test class for LogDouble.
 * 
 * @author Isaac Arvestad
 */
public class LogDoubleTest {

	/**
	 * Tests that the correct sign is returned for numbers between -1.0 and 1.0.
	 */
	@Test
	public void testGetSign() {
		for (double i = -1.0; i < 1.0; i += 0.01) {
			LogDouble value = new LogDouble(i);

			if (i < 0) {
				assertTrue("getSign() should return -1", value.getSign() == -1);
			} else if (i == 0) {
				assertTrue("getSign() should return 0", value.getSign() == 0);
			} else {
				assertTrue("getSign() should return 1", value.getSign() == 1);
			}
		}
	}
}
