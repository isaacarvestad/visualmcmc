package se.cbb.vmcmc;

import java.lang.reflect.Field;
import java.io.PrintStream;
import java.io.File;
import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import se.cbb.vmcmc.computations.TrueTreePresence;
import se.cbb.vmcmc.gui.MCMCApplication;
import se.cbb.vmcmc.main.MCMCCommandLine;
import se.cbb.vmcmc.main.VersionInfo;
import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCFileReader;
import se.cbb.vmcmc.libs.Parameters;
import se.cbb.vmcmc.writers.jprimewriter.MrBayes2JPrIMe;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterDescription;

public class VMCMCStarter {

	public String getAppName() {
		return "VMCMC";
	}

	/** Definition: 			Main function for VMCMC.										
	<p>Usage: 				Initialize the application from command line.						
 	<p>Function:			Gets the inputs from command line, parses them and calls the appropriate constructor of MCMCWindow. 				
 	<p>Classes:				Parameters, JCommander, JCommanderUserWrapper, Triple.
 	<p>Internal Functions: 	MCMCApplication(),  		
 	@return 				(A new graphical window)/(command line) statistical and/or convergence test analysis.					
	 */
	public static void main(String[] args) {
		/* Function Variables and Initialization */
		Parameters params = new Parameters();
		JCommander commander;

		System.setProperty("apple.laf.useScreenMenuBar", "true"); // Put the app menus in Mac's menubar
		System.setProperty("apple.awt.application.name", "VMCMC"); // Make Mac OS understand that this is the actual application name (instead of this class name of the main method).
		
		try {
			commander = new JCommander(params, args);
			/* Print the help menu */
			if (params.help) {
				printHelpAndUsage(params, commander);
				return;
			}
		}
		catch (Exception e) {
			System.err.println("Error! " + e.getMessage());
			System.exit(-1);
		}

		try {
			if (params.showVersion) {
				VersionInfo v = new VersionInfo();
				System.out.println("VMCMC version " + v.getVersion());
				System.exit(0);
			}
			
			if (args.length == 0)                           
				new MCMCApplication();
			else if ((args.length == 1) && (params.nogui == false) && (params.test == false) && (params.stats == false) && (params.ess == false) && (params.geweke == false) && (params.gr == false) && (params.posterior == false) && (params.convergencetest == false) && (params.maptree == false) && (params.sampledata == false) && (params.trueTest == false) && (params.treeconvergence == false)) {
			 	// Run the VMCMC GUI directly with the specified file as input. Useful when analyzing file in GUI directly from command line.
				new MCMCApplication(params.files.get(0));
			}
			else {
				String path = params.path;
				if(!path.endsWith("/"))
					path = path + "/";

				if(!params.paramfile.equals(""))
					params = new Parameters(); // Does this actually do something? Isn't the params.paramfile needed somehow? /arve

				considerRedirectingStdout(params); // From here on, always write to stdout!
				
				runVMCMCUsingCommandLineParams(params, path, params.alpha, Integer.parseInt(params.burnin1), Integer.parseInt(params.burnin2));
			}
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
			System.exit(-1);
		}
	}

	/*
	 * considerRedirectingStdout
	 * 
	 * If requested, change the binding of System.out to the named file.
	 */
	private static void considerRedirectingStdout(Parameters params) {
		if (!params.outFile.equals("stdout")) {
			try {
				System.setOut(new PrintStream(new File(params.outFile)));
			}
			catch (Exception e) {
				System.err.println("Cannot write to file '" + params.outFile + "'. Aborting.");
				System.exit(-3);
			}
		}
	}


	private static void runVMCMCUsingCommandLineParams(Parameters params, String path, Double alpha, int burnin1, int burnin2) throws Exception {
		// Filename not provided. All functions expect a file to work with.
		if (params.files.size() < 1) {
			throw new Exception("No filename given. When starting VMCMC on the commandline, an input file is always needed.");
		} 
		
		if (params.files.size() > 2) {
			throw new Exception("Too many file names given. VMCMC can only handle one or two trace files.");
		}

		// Convert a MrBayes MCMC file with .p extension and (optionally a .t file) to its tab separated jprime formatted file.
		else if (params.convertMrBayes2jprime == true) 
			MrBayes2JPrIMe.main(params.files.get(0), path, params.outFile);

		// non-GUI version of parallel chain analysis for two chains run in parallel. Runs when two filenames are provided.
		else if(params.files.size() == 2) {
			try {
				assert burnin1 >= 0;
				assert burnin2 >= 0;
				MCMCCommandLine.parallelChainAnalysis(burnin1, burnin2, alpha, params.files.get(0), params.files.get(1));
				// We should also have call that goes straight to MCMCCommandLine.parallelConvergenceTest, without a full analysis.
			}
			catch (Exception e) {
				System.err.println("Error! Something went wrong in a parallel chain analysis.");
				System.err.println("Error message: " + e.getMessage());
				System.exit(-1);
			}

		}
		else { // Only one input trace file 
			String input_trace_filename = params.files.get(0);
			assert input_trace_filename != null; //Sanity check
			MCMCDataContainer data = null;
			try {
				File f = new File(input_trace_filename);
				data = MCMCFileReader.readMCMCFile(f, false, false);
			}
			catch (Exception e) {
				System.err.println("Error! Could not read the trace file.");
				System.err.println(e.getMessage());
				System.exit(-2);
			}
			assert data != null;

			// Compute the posterior of MCMC chain for the tree parameter (without branch lengths or after removal of branch lengths) in the MCMC run.
			if(params.posterior == true) {
				//MCMCCommandLine.execute(10, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
				int burnin = data.calculateBurnin(params.burnin);
				MCMCCommandLine.printTreePosterior(data, burnin);
			} 

			// Determine the maximum a posteriori (MAP) tree for the tree (without branch length) for the tree parameter in the MCMC run.
			else if (params.maptree == true) {
				//MCMCCommandLine.execute(11, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
				int burnin = data.calculateBurnin(params.burnin);
				MCMCCommandLine.determineMaximumAPosterioriTree(data, burnin);
			} 
			else if (params.consensustrees) {
				int burnin = data.calculateBurnin(params.burnin);
				MCMCCommandLine.determineConsensusTrees(data, burnin);
			}

			// Wrong (copy-n-pasted) comment was here... 
			else if (params.treeconvergence == true) {
				//MCMCCommandLine.execute(12, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
				int burnin = data.calculateBurnin(params.burnin);
				MCMCCommandLine.printTreeConvergenceTest(data, burnin, params.alpha);
			} 


			// Analysis estimates for VMCMC 2. To be removed on completion of convergence estimate analysis from the final version of VMCMC.
			else if (params.trueTest == true) 
				TrueTreePresence.generateScriptForPosterior(path, "essstandardized");

			// Compute the results of convergence tests only.
			else if(params.convergencetest == true) 
				//MCMCCommandLine.execute(8, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
				MCMCCommandLine.printConvergenceTestResults(data);	

				// Pick one sample data after removing burnin (estimated using ESS-last).
			else if (params.sampledata == true) {
					//MCMCCommandLine.execute(9, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
					int burnin = data.calculateBurnin(params.burnin);
					MCMCCommandLine.pickAndWriteTraceSample(data, burnin);

				}
				// Compute the results of all convergence tests only.
				else if (params.test == true)
					MCMCCommandLine.execute(2, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);

				// Compute the parameter statistics only.
				else if (params.stats == true) {
					//MCMCCommandLine.commandline(3, input_trace_file, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
					// Needs a burnin.
					int burnin = data.calculateBurnin(params.burnin);
					MCMCCommandLine.printSimpleStats(data, burnin, params.confidence, alpha, burnin1);
				}

				// Estimate burn-in values for all parameters using Geweke.
				else if (params.geweke == true)
					MCMCCommandLine.execute(4, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);

				// Estimate burn-in values for all parameters using ESS.
				else if (params.ess == true)
					MCMCCommandLine.execute(5, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);

				// EStimate burn-in values for all parameters using Gelman Rubin.
				else if (params.gr == true)
					MCMCCommandLine.execute(6, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);

			// Compute all parameter statistics and convergence tests estimates and display in the command line.
			else if (params.nogui == true) { // TODO: This test should come last. We do not want the --nogui option to exclude other analysis. /arve
				MCMCCommandLine.execute(7, data, params.burnin, params.confidence, path, params.outFile, alpha, burnin1);
				System.out.println("\n\t}\n}");
			} 

				System.out.println("}"); 
			}

	}
	
	
	private static void printHelpAndUsage(Parameters params,
			JCommander commander) {
		VersionInfo v = new VersionInfo();
		StringBuilder sb = new StringBuilder(65536);
		sb.append("VMCMC version " + v.getVersion() + "\n");
		sb.append("USAGE:\n\tjava [-Xmx2000m -Xms1800m] -jar VMCMC-x.y.z.jar").append('\n');
		sb.append("  Single Chain:   \n\tjava [-Xmx2000m -Xms1800m] -jar VMCMC-x.y.z.jar [options] <filename> ").append('\n');
		sb.append("  Parallel Chains:\n\tjava [-Xmx2000m -Xms1800m] -jar VMCMC-x.y.z.jar [options] <filename1> <filename2> ").append('\n');
		

		ParameterDescription mainParam = commander.getMainParameter();
		if (mainParam != null) {
			sb.append("\nOPTIONS:\n");
			sb.append("     ").append(mainParam.getDescription()).append('\n');
		}
		List<ParameterDescription> params1 = commander.getParameters();
		Field[] fields = params.getClass().getFields();
		for (Field f : fields) {
			for (ParameterDescription p : params1) {
				if (f.getName().equals(p.getField().getName())) {
					sb.append(p.getNames()).append('\n');
					String def = (p.getDefault() == null ? "" : " Default: " + p.getDefault().toString() + '.');
					String desc = WordUtils.wrap(p.getDescription() + def, 120);
					desc = "     " + desc;
					desc = desc.replaceAll("\n", "\n     ") + '\n';
					sb.append(desc);
					break;
				}
			}
		}
		System.out.println(sb.toString());
	}

}


