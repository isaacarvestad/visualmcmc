package se.cbb.vmcmc.libs;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.IParameterValidator;

public class ParameterValidatorUnitInterval implements IParameterValidator {
	public void validate(String name, String arg) throws ParameterException {
		double value = Double.parseDouble(arg);
		if (value < 0.0 || value > 1.0) {
			throw new ParameterException("Parameter " + name + " is not in the interval [0,1]!");
		}
	}
}
