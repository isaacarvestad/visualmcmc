package se.cbb.vmcmc.libs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import se.cbb.vmcmc.gui.MCMCDisplayPanel;
import se.cbb.vmcmc.gui.MCMCGraphPanel;
import se.cbb.vmcmc.gui.MCMCGraphToolPanel;
import se.cbb.vmcmc.gui.MCMCGraphToolParallelPanel;
import se.cbb.vmcmc.gui.MCMCMainTab;
import se.cbb.vmcmc.gui.MCMCOverviewTab;
import se.cbb.vmcmc.gui.MCMCParallelMainTab;
import se.cbb.vmcmc.gui.MCMCStandardTab;
import se.cbb.vmcmc.gui.MCMCTableTab;
import se.cbb.vmcmc.gui.MCMCWindow;

public class MCMCNumericFunctions {
	private static ArrayList<Integer> EssMaxList;
	private static ArrayList<Integer> gewekeMaxList;
	/** Definition: 			Creates and updates new instance of MCMCMainTab and adds default components. Sets application specific default values (including burnin - 10%).						
 	<p>Function:			Handle MCMCMainTab and set default values for it. Displays all the statistics and convergence test results for a parameter.				
 	<p>Classes:				MCMCMainTab, MCMCDataContainer, MCMCGraphToolPanel, 												
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static MCMCMainTab createMainPanel(final MCMCDataContainer datacontainer, MCMCWindow parent) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCMainTab 		mainPanel 	= new MCMCMainTab(parent, EssMaxList, gewekeMaxList);
		MCMCGraphToolPanel 		graphtoolPanel;
		JComboBox 				droplist;
		int numSeries;
		int overallConvergenceESS;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		graphtoolPanel = mainPanel.getGraphTool();
		droplist = mainPanel.getDropList();
		overallConvergenceESS = 0;
		
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.setDataContainer(datacontainer);
		
		if(datacontainer != null) {
			numSeries = datacontainer.getNumValueSeries();
			if(numSeries != 0) {
				if(datacontainer.getValueSerie(0).size() < 100)
					overallConvergenceESS = -1;
				else {
					for (int i  : EssMaxList) {
						if(i < 0) {
							overallConvergenceESS = -1;
							break;
						} else if (i > overallConvergenceESS) 
							overallConvergenceESS = i;
					}
				}
			} 
			if(overallConvergenceESS == -1) {
				mainPanel.setBurnIn(0);
				mainPanel.updateDisplayPanels();
				graphtoolPanel.setDataContainer(datacontainer);
				graphtoolPanel.setSeriesID(0);
				graphtoolPanel.setBurnIn(0);
			} else {
				double burninpercent = (double)overallConvergenceESS/(double)datacontainer.getNumLines();
				mainPanel.setBurnIn(burninpercent);
				mainPanel.updateDisplayPanels();
				graphtoolPanel.setDataContainer(datacontainer);
				graphtoolPanel.setSeriesID(0);
				graphtoolPanel.setBurnIn(burninpercent);
			}
			graphtoolPanel.updateGraph();
			graphtoolPanel.updateRuler();
			droplist.setModel(new DefaultComboBoxModel(datacontainer.getValueNames().toArray())); 
			droplist.setSelectedIndex(0);
		}

		//Slider listener - only updates components available in main.
		mainPanel.getGraphTool().getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider source 	= (JSlider) arg0.getSource();
				double burnin 	= (double) source.getValue()/source.getMaximum();

				mainPanel.setBurnIn(burnin);
				mainPanel.updateDisplayPanels();
			}
		});

		//Burn in field listener - updates burnin for main panel based on text field value. 
		mainPanel.getBurnInField().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int serieSize 	= datacontainer.getValueSerie(mainPanel.getSeriesID()).size();
				int fieldValue 	= 0;

				try {
					fieldValue 	= Integer.valueOf(mainPanel.getBurnInField().getText());
				} catch (NumberFormatException e) {}

				double burnin 	= (double) fieldValue/serieSize;

				mainPanel.setBurnIn(burnin);
				mainPanel.getGraphTool().setBurnIn(burnin);
				mainPanel.getGraphTool().updateGraph();

				mainPanel.updateDisplayPanels();
			}
		});

		//Droplist listener - will update seriesID for mainpanel and it's components.
		mainPanel.getDropList().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int seriesID = mainPanel.getDropList().getSelectedIndex();
				datacontainer.setSelectedIndex(seriesID);
				MCMCGraphToolPanel graphTool = mainPanel.getGraphTool();

				mainPanel.setSeriesID(seriesID);
				graphTool.setSeriesID(seriesID);
				graphTool.updateGraph();
				graphTool.updateRuler();

				mainPanel.updateDisplayPanels();
			}
		});

		return mainPanel;

		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static MCMCOverviewTab createOverviewPanel(final MCMCDataContainer datacontainer, MCMCWindow parent) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCOverviewTab 	tablePanel 	= new MCMCOverviewTab(datacontainer);
		ArrayList<Integer> 		EssList;
		ArrayList<Integer> 		GewekeList;
		ArrayList<Integer> 		GelmanRubinList;
		ArrayList<String> 		EssListString;
		ArrayList<String> 		GelmanRubinListString;
		ArrayList<String> 		GewekeListString;
		int 					numValues;
		int 					EssMax;
		int 					GewekeMax;
		int 					overallConvergenceGR;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		EssList					= new ArrayList<Integer>();
		GewekeList				= new ArrayList<Integer>();
		GelmanRubinList			= new ArrayList<Integer>();
		GelmanRubinListString   = new ArrayList<String>();
		GewekeListString   		= new ArrayList<String>();
		EssListString   		= new ArrayList<String>();
		numValues 				= datacontainer.getNumValueSeries();
		EssMax 					= 0;
		GewekeMax 				= 0;
		overallConvergenceGR 	= 0;
		
		/* ******************** FUNCTION BODY ************************************* */
				
		tablePanel.addColumn("Parameters", datacontainer.getValueNames());
		
		for (int i = 0; i < numValues; i++) 
			EssList.add(MCMCMath.calculateESSmax(datacontainer.getValueSerie(i).toArray()));
		EssMaxList = EssList;
		for (int i = 0; i < numValues; i++) {
			if(EssList.get(i) >= 0)
				EssListString.add(String.valueOf(EssList.get(i)));
			else
				EssListString.add("not converged");
		}
		tablePanel.addColumn("ESS Max (SHEB)", EssListString);
		
		for (int i = 0; i < numValues; i++)
			GewekeList.add(MCMCMath.calculateGeweke(datacontainer.getValueSerie(i).toArray()));
		gewekeMaxList = GewekeList;
		for (int i = 0; i < numValues; i++) {
			if(GewekeList.get(i) > 0)
				GewekeListString.add(String.valueOf(GewekeList.get(i)));
			else
				GewekeListString.add("not converged");
		}
		tablePanel.addColumn("Parameter Geweke", GewekeListString);
		
		for(int i = 0; i < numValues; i++) {
			if(EssList.get(i) < 0) {
				EssMax = -1;
				break;
			}
			if(EssList.get(i) > EssMax) 
				EssMax = EssList.get(i);
		}
		
		for(int i = 0; i < numValues; i++) {
			if(GewekeList.get(i) <= 0) {
				GewekeMax = Integer.MIN_VALUE;
				break;
			} else if(GewekeList.get(i) > GewekeMax) 
				GewekeMax = GewekeList.get(i);
		}
		
		for(int i = 0; i < numValues; i++) {
			int originalBurnin = 0;
			Object[] serie = datacontainer.getValueSerie(i).toArray();
			boolean gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
			while(gelmanRubin != true && originalBurnin < (0.5 * serie.length)) {
				originalBurnin = originalBurnin + 1;
				gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
			}
			if(gelmanRubin == true)	{
				GelmanRubinList.add(originalBurnin);
				if(overallConvergenceGR != -1 && originalBurnin > overallConvergenceGR)
					overallConvergenceGR = originalBurnin;
			} else {
				GelmanRubinList.add(Integer.MIN_VALUE);
				overallConvergenceGR = -1;
			}
		}
		
		tablePanel.setEssMax(EssMax);
		tablePanel.setGeweke(GewekeMax);
		tablePanel.setConvergeneceGR(overallConvergenceGR);

		for(int i = 0; i < numValues; i++) {
			if(GelmanRubinList.get(i) != Integer.MIN_VALUE)
				GelmanRubinListString.add(String.valueOf(GelmanRubinList.get(i)));
			else
				GelmanRubinListString.add("not converged");
		}
		tablePanel.addColumn("Parameter Gelman-Rubin", GelmanRubinListString);
		return tablePanel;
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static MCMCParallelMainTab createMainPanelforParallel(final MCMCDataContainer datacontainer, final MCMCWindow parent) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCParallelMainTab 		mainPanel 	= new MCMCParallelMainTab(parent);
		MCMCGraphToolParallelPanel 		graphtoolPanel;
		JComboBox 				droplist;
		JComboBox 				alphadroplist;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		graphtoolPanel = mainPanel.getGraphTool();
		droplist = mainPanel.getDropList();
		alphadroplist = mainPanel.getAlphaDropList();
		
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.setDataContainer(datacontainer);
		
		if(datacontainer != null) {
			mainPanel.setBurnIn(0);
			String[] strArr = {"1% Significance Level", "5% Significance Level", "10% Significance Level"};
			alphadroplist.setModel(new DefaultComboBoxModel(strArr)); 
			alphadroplist.setSelectedIndex(1);
			mainPanel.updateDisplayPanels();
			graphtoolPanel.setDataContainer(parent.getDataContainer(1), 1);
			graphtoolPanel.setDataContainer(parent.getDataContainer(2), 2);
			graphtoolPanel.setSeriesID(0);
			
			int burnin = parent.getDataContainer(1).getBurnin();
			int numPoints = parent.getDataContainer(1).getNumLines();
			if(burnin < parent.getDataContainer(2).getBurnin()) {
				burnin = parent.getDataContainer(2).getBurnin();
				numPoints = parent.getDataContainer(2).getNumLines();
			}
			double burn = (double)burnin/(double)numPoints;
			graphtoolPanel.setBurnIn(burn);

			graphtoolPanel.updateGraph();
			graphtoolPanel.updateRuler();
			droplist.setModel(new DefaultComboBoxModel(datacontainer.getValueNames().toArray())); 
			droplist.setSelectedIndex(0);
		}

		//Slider listener - only updates components available in main.
		mainPanel.getGraphTool().getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider source 	= (JSlider) arg0.getSource();
				double burnin 	= (double) source.getValue()/source.getMaximum();

				mainPanel.setBurnIn(burnin);
				mainPanel.updateDisplayPanels();
			}
		});

		//Burn in field listener - updates burnin for main panel based on text field value. 
		mainPanel.getBurnInField().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double burnin;
				int serieSize;
				int fieldValue = 0;
				if(parent.getDataContainer(1).getBurnin() < parent.getDataContainer(2).getBurnin()) {
					serieSize 	= parent.getDataContainer(2).getValueSerie(mainPanel.getSeriesID()).size();
					try {
						fieldValue 	= Integer.valueOf(mainPanel.getBurnInField().getText());
					} catch (NumberFormatException e) {}

					burnin 	= (double) fieldValue/serieSize;
				} else {
					serieSize 	= parent.getDataContainer(1).getValueSerie(mainPanel.getSeriesID()).size();
					try {
						fieldValue 	= Integer.valueOf(mainPanel.getBurnInField().getText());
					} catch (NumberFormatException e) {}
				}
				burnin 	= (double) fieldValue/serieSize;
				mainPanel.setBurnIn(burnin);
				mainPanel.getGraphTool().setBurnIn(burnin);
				mainPanel.getGraphTool().updateGraph();

				mainPanel.updateDisplayPanels();
				
			}
		});
		
		mainPanel.getAlphaDropList().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mainPanel.getAlphaDropList().getSelectedIndex() == 0)
					mainPanel.setAlpha(0.01);
				else if(mainPanel.getAlphaDropList().getSelectedIndex() == 1)
					mainPanel.setAlpha(0.05);
				else
					mainPanel.setAlpha(0.1);
				mainPanel.updateWilcoxonRankPanel();
			}
		});

		//Droplist listener - will update seriesID for mainpanel and it's components.
		mainPanel.getDropList().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int seriesID = mainPanel.getDropList().getSelectedIndex();
				datacontainer.setSelectedIndex(seriesID);
				parent.getDataContainer(1).setSelectedIndex(seriesID);
				parent.getDataContainer(2).setSelectedIndex(seriesID);
				MCMCGraphToolParallelPanel graphTool = mainPanel.getGraphTool();

				mainPanel.setSeriesID(seriesID);
				graphTool.setSeriesID(seriesID);
				graphTool.updateGraph();
				graphTool.updateRuler();

				mainPanel.updateDisplayPanels();
			}
		});

		return mainPanel;

		/* ******************** END OF FUNCTION *********************************** */
	}


	/** Definition: 			Adds functionalty between Main tab and	the JTabbedPane.									
	<p>Usage: 				When data in an interval is to be extracted and examined.						
 	<p>Function:			Extract the interval from the graph and display in a new pane. 				
 	<p>Classes:				MCMCMainTab.  		
	 */
	public static void linkMainToTabs(final MCMCMainTab mainPanel, final JTabbedPane tabs, final MCMCWindow parent) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		JButton 				extractSelectionButton;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		extractSelectionButton 	= new JButton("Extract interval");
		
		/* ******************** FUNCTION BODY ************************************* */
		extractSelectionButton.setBackground(Color.WHITE);
		extractSelectionButton.setToolTipText("Select an interval on the graph by left click and drag on the graph pane above and selected interval will be extracted in a seperate tab.");

		extractSelectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					MCMCDataContainer datacontainer = mainPanel.getDataContainer();
					MCMCGraphToolPanel graphTool = mainPanel.getGraphTool();
					MCMCGraphPanel graph = graphTool.getGraph();

					int first = (int) (((double) graph.getSelection().getLeftPos()/graphTool.getScrollPane().getWidth())*datacontainer.getNumLines());
					int last = (int) (((double) graph.getSelection().getRightPos()/graphTool.getScrollPane().getWidth())*datacontainer.getNumLines());

					MCMCMainTab submainPanel = createMainPanel(datacontainer.getSubDataContainer(first, last), parent);

					tabs.add("Graph " + first + " - " + last, submainPanel);

					mainPanel.updateDisplayPanels();
					mainPanel.getGraphTool().updateGraph();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Select an interval first!\nCan not extract interval before selection. \nSelect interval on the graph panel above first!", "Extract Interval error", JOptionPane.YES_OPTION);
				}
			}
		});

		mainPanel.addToSouth(extractSelectionButton);
		mainPanel.addToSouth(Box.createRigidArea(new Dimension(10, 0)));
		
		/* ******************** END OF FUNCTION *********************************** */
	}


	/** Definition: 			Adds functionalty between Main tab and the Table tab.								
	<p>Usage: 				Select a parameter to see its data.						
 	<p>Function:			For a selected parameter in the main tab, display its corresponding data in table panel. 				
 	<p>Classes:				MCMCMainTab, MCMCTableTab.  													
	 */
	public static void linkMainToTable(final MCMCMainTab mainPanel, final MCMCTableTab tablePanel) {
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.getDropList().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int seriesID = mainPanel.getDropList().getSelectedIndex();
				
				if(tablePanel != null) {
					tablePanel.setSelectedButton(seriesID);
				}
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** Definition: 			Creates and updates new instance of MCMCTableTab and adds default components.										
	<p>Usage: 				Display the data in a tabular format. 						
 	<p>Function:			Adds the parameter names and parameter values to the Table Tab (Second Tab) 				
 	<p>Classes:				MCMCTabletab, MCMCDataContainer.
	 */
	public static MCMCTableTab createTablePanel(final MCMCDataContainer datacontainer) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCTableTab 		tablePanel 	= new MCMCTableTab();
		ArrayList<String> 		names;
		int 					numValues;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		names 								= datacontainer.getValueNames();
		numValues 							= datacontainer.getNumValueSeries();
		
		/* ******************** FUNCTION BODY ************************************* */
		tablePanel.setDataContainer(datacontainer);
		for(int i = 0; i < numValues; i++) 
			tablePanel.addColumn(names.get(i), datacontainer.getValueSerie(i));
		tablePanel.adjustLengths();
		return tablePanel;
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** Definition: 			Adds functionality between Table tab and the Main tab.						
 	<p>Function:			Create the left table tab on the main window for selecting the parameter.				
 	<p>Classes:				MCMCTableTab, MCMCMainTab, MCMCDataContainer.  													
	 */
	public static void linkTableToMain(final MCMCTableTab tablePanel, final MCMCMainTab mainPanel) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		MCMCDataContainer 		datacontainer;
		int 					numValues;
		JRadioButton[] 			buttons;
		JPanel 					buttonPanel;
		ButtonGroup 			buttonGroup;
		JScrollPane 			scrollpane;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		datacontainer 			= tablePanel.getDataContainer();
		numValues 				= datacontainer.getNumValueSeries();
		buttons 				= new JRadioButton[numValues];
		buttonPanel 			= new JPanel();
		buttonGroup 			= new ButtonGroup();
		
		/* ******************** FUNCTION BODY ************************************* */
		tablePanel.setButtons(buttons);
		buttonPanel.setLayout(new GridLayout(1, 0));
		buttonPanel.setBackground(new Color(0xFFEEEEFF));

		for(int i=0; i<numValues; i++) {
			buttons[i] = new JRadioButton();
			buttons[i].setBackground(new Color(0xFFEEEEFF));

			final int id = i;

			buttons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mainPanel.getDropList().setSelectedIndex(id);
					mainPanel.updateDisplayPanels();
				}
			});

			JPanel panel = new JPanel();
			panel.setBackground(new Color(0xFFEEEEFF));
			panel.add(buttons[i]);

			buttonPanel.add(panel);
			buttonGroup.add(buttons[i]);
		}
		
		scrollpane = new JScrollPane(tablePanel.getTable(), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		tablePanel.add(scrollpane);
		tablePanel.addToNorth(buttonPanel);

		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** Definition: 			Adds functionality between Table tab and the Main tab.						
 	<p>Function:			Create the left table tab on the main window for selecting the parameter.				
 	<p>Classes:				MCMCTableTab, MCMCMainTab, MCMCDataContainer.  													
	 */
	public static void linkOverviewToMain(final MCMCOverviewTab tablePanel) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		JScrollPane 			scrollpane;
		MCMCDisplayPanel 		scrollpane1;
		MCMCDisplayPanel 		scrollpane2;
		MCMCStandardTab			temp;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		temp = new MCMCStandardTab();
		/* ******************** FUNCTION BODY ************************************* */
		
		scrollpane = new JScrollPane(tablePanel.getTable());
		scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		tablePanel.add(scrollpane);
		tablePanel.updateDisplayPanels();
		scrollpane1 = tablePanel.getOverallConvergencePanel();
		scrollpane2 = tablePanel.getOverallBurninPanel();
		
		temp.addToWest(scrollpane1);
		temp.addToWest(scrollpane2);
		temp.add(scrollpane);
		
		tablePanel.add(temp);

		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static void linkTableToMaininParallel(final MCMCTableTab tablePanel, final MCMCParallelMainTab mainPanel) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		JScrollPane 			scrollpane;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		/* ******************** FUNCTION BODY ************************************* */
		scrollpane = new JScrollPane(tablePanel.getTable());
		scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		tablePanel.add(scrollpane);

		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static MCMCDataContainer combineTwoContainers(MCMCDataContainer container1, MCMCDataContainer container2) {
		MCMCDataContainer datacontainer = new MCMCDataContainer();
		
		if(container1.getBurnin() == 0 || container2.getBurnin() == 0)
			return null;
		
		datacontainer.setFileName("Pooled Runs");
		datacontainer.setSerieTypes(container1.getSerieTypes());
		datacontainer.setNumLines(container1.getNumLines() + container2.getNumLines());

		for(int i=0; i<container1.getValueNames().size(); i++) {
			datacontainer.addValueName(container1.getValueNames().get(i));
			List<Double> temp = new ArrayList<Double>();
			temp.addAll(container1.getValueSerie(i));
			temp.addAll(container2.getValueSerie(i));
			datacontainer.addValueSerie(temp);
		}
		
		for(int j = 0; j < container1.getNumTreeSeries(); j++) {
			datacontainer.addTreeName(container1.getTreeNames().get(j));
			ArrayList<MCMCTree> temp = new ArrayList<MCMCTree>();
			temp.addAll(container1.getTreeSerie(j));
			temp.addAll(container2.getTreeSerie(j));
			datacontainer.addTreeSerie(temp);
		}

		return datacontainer;
	}
	
	public static void analyzeBothChains(List<Double> series1, List<Double> series2, int burnin1, int burnin2) {
		double sample1 []  = new double[series1.size() - burnin1] ;
		double sample2 []  = new double[series2.size() - burnin2] ;
		for(int i = 0 ; i < series1.size() - burnin1; i++) 
			sample1[i] = series1.get(i + burnin1);
		for(int i=0 ; i < series2.size() - burnin2; i++) 
			sample2[i] = series2.get(i + burnin2);	
		
		// Mann-Whitney U test (also called Wilcoxon rank-sum test).
		System.out.println();
		System.out.println("Non Parametric Hypothesis Testing");
		System.out.println("Testing of Hypothesis with Wilcoxon rank-sum test:");
		
		MannWhitneyUTest mw  = new MannWhitneyUTest();
		double testStatistic = mw.mannWhitneyU(sample1,sample2);
		double pValue  = mw.mannWhitneyUTest(sample1,sample2);
		
		System.out.println("The Test statistics of Wilcoxon rank-sum test = " + testStatistic);
		System.out.println("The p-value = " + pValue);
		
		// Setting Level of significance at 1%  = 0.001 , 5%  = 0.05 , 10% = 0.01
		double alpha = 0.05;
		System.out.println("Level of significance = " + alpha);
		
		// Testing of hypothesis decision:
		System.out.println("-----------------------------------------------------------");
		System.out.print("Testing of hypothesis decision: ");
		if(pValue < alpha )
			System.out.print("Reject the null hypothesis");
		else
			System.out.print("Accept the null hypothesis");
	}
	
	public static void analyzeBothChainsWithChiSquare(List<Double> series1, List<Double> series2, int burnin1, int burnin2) {
		double sample1 []  = new double[series1.size() - burnin1] ;
		double sample2 []  = new double[series2.size() - burnin2] ;
		for(int i = 0 ; i < series1.size() - burnin1; i++) 
			sample1[i] = series1.get(i + burnin1);
		for(int i=0 ; i < series2.size() - burnin2; i++) 
			sample2[i] = series2.get(i + burnin2);	
		
		// Mann-Whitney U test (also called Wilcoxon rank-sum test).
		System.out.println();
		System.out.println("Non Parametric Hypothesis Testing");
		System.out.println("Testing of Hypothesis with Chi Square test:");
		
		double min1 = sample1[0];
		double max1 = sample1[0];
		for(int i = 1; i < sample1.length; i++) {
			if(sample1[i] < min1)
				min1 = sample1[i];
			if(sample1[i] > max1)
				max1 = sample1[i];
		}
		
		for(int j = 0; j < sample2.length; j++) {
			if(sample2[j] < min1)
				min1 = sample2[j];
			if(sample2[j] > max1)
				max1 = sample2[j];
		}
		
		int numberofbins = (sample2.length / 100) + 1;
		
		double[] binvalues = new double[numberofbins];
		for(int i = 0; i < numberofbins; i++) {
			binvalues[i] = min1 + (i * ((max1-min1)/numberofbins));
		}
		
		int[] s1 = new int[numberofbins];
		int[] s2 = new int[numberofbins];
		
		for(int i = 0; i < numberofbins; i++) {
			s1[i] = 0;
			s2[i] = 0;
		}
		
		for(int i = 0; i < sample1.length; i++) {
			for (int j = 1; j < numberofbins; j++) {
				if(sample1[i] >= binvalues[j-1] && sample1[i] < binvalues[j]) {
					s1[j] = s1[j] + 1;
					break;
				}
			}
		}
		
		for(int i = 0; i < sample2.length; i++) {
			for (int j = 1; j < numberofbins; j++) {
				if(sample2[i] >= binvalues[j-1] && sample2[i] < binvalues[j]) {
					s2[j] = s2[j] + 1;
					break;
				}
			}
		}
		
		printHists(s1,binvalues);
		printHists(s2,binvalues);
		
/*		ChiSquareTest chi  = new ChiSquareTest();

		ArrayList<String> result = new ArrayList<String>();
		double testStatistic = chi.chiSquareDataSetsComparison(s1,s2);
		double pValue = chi.chiSquareTestDataSetsComparison(s1,s2);
		
		System.out.println("The Test statistics of Chi Square test = " + testStatistic);
		System.out.println("The p-value = " + pValue);
		
		// Setting Level of significance at 1%  = 0.001 , 5%  = 0.05 , 10% = 0.01
		double alpha = 0.05;
		System.out.println("Level of significance = " + alpha);
		
		// Testing of hypothesis decision:
		System.out.println("-----------------------------------------------------------");
		System.out.print("Testing of hypothesis decision: ");
		if(pValue < alpha )
			System.out.print("Reject the null hypothesis");
		else
			System.out.print("Accept the null hypothesis");*/
	}
	
	public static void printHists(int[] frequencyArray, double[] binValues) {
		for(int i = 0; i < binValues.length; i++) {
			System.out.println(binValues[i] + "\t" + frequencyArray[i]);
		}
		System.out.println("------------------------------------");
	}
	
	public static void readImageFiles(JPanel imagePanel, MCMCWindow window, String fileName) {
		InputStream is = window.getClass().getResourceAsStream(fileName);
    	if(is.equals(null)) {
    		System.out.println("File not found");
    		System.exit(-1);
    	}
    	try {
    		File f1 = File.createTempFile("image", ".png");
    		OutputStream out = new FileOutputStream(f1);
    		byte buf[]=new byte[1024];
    		int len;
    		while((len=is.read(buf))>0)
    			out.write(buf,0,len);
    		out.close();
    		is.close();
    		imagePanel.add(new JLabel(new ImageIcon(ImageIO.read(f1))));
    	} catch (IOException e){
    		System.err.println("Warning: Image not found. Continuing without image.");
    	}
	}
	
	public static ArrayList<Integer> getEssMax() { return EssMaxList; }
	public static ArrayList<Integer> getGewekeMax() { return gewekeMaxList; }
	
}
