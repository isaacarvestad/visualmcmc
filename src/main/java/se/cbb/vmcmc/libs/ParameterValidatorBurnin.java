package se.cbb.vmcmc.libs;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.IParameterValidator;

public class ParameterValidatorBurnin implements IParameterValidator {
	public void validate(String name, String arg) throws ParameterException {
		if (arg == null) return; // We accept this as the default value. 
		if (arg.equalsIgnoreCase("maxess") || arg.equalsIgnoreCase("ess")) return;
		if (arg.equalsIgnoreCase("geweke") || arg.equalsIgnoreCase("g")) return;
		if (arg.equalsIgnoreCase("gelmanrubin") || arg.equalsIgnoreCase("gr")) return;
		if (arg.equalsIgnoreCase("ness") || arg.equalsIgnoreCase("sess")) return; 
		
		try {
			int x = Integer.parseInt(arg);
			if (x < 0) {
					throw new ParameterException("Parameter " + name + " cannot be negative (here: " + arg + ").");
			} else {
				return;
			}
		}
		catch (NumberFormatException e1) {
			try {
				double x = Double.parseDouble(arg);
				if (x < 0.0 || x > 1.0) {
					throw new ParameterException("Parameter " + name + " must be an integer, or in the unit interval (here: " + arg + ").");
				}
			}
			catch (NumberFormatException e2) {
				throw new ParameterException("Could not parse the burnin option: " + arg);
			}
		}
	}
}