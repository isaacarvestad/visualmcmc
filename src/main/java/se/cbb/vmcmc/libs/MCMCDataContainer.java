package se.cbb.vmcmc.libs;

import java.util.ArrayList;
import java.util.List;
import java.lang.Character;


/**
 * SerieType: Enumeration of all types of parameters handled by datacontainer.
 */
enum SerieType {
	FLOAT,
	TREE,
	INT,
	OTHER
}

public class MCMCDataContainer {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	String 							fileName;		//Name of file
	int 							numSeries;		//Total number of series
	int 							numTreeSeries; 	//Number of tree series
	int 							numValueSeries;	//Number of numerical series
	int 							numLines;		//Number of lines in file
	ArrayList<SerieType> 			listSerietype;	//List of parameter types
	ArrayList<List<Double>> 		listValueSeries;
	ArrayList<String> 				listValueNames;	//List of names for numerical series
	ArrayList<Integer> 				listNumValues;
	ArrayList<ArrayList<MCMCTree>> 	listTreeSeries;
	ArrayList<String> 				listTreeNames;	//List of names for tree series
	ArrayList<Integer> 				listNumTrees;
	int 							burnin;
	int indexForC1;
	int indexTreeSeriesForC1;

	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MCMCDataContainer(){
		listSerietype 		= new ArrayList<SerieType>();	//List of parameter types
		listValueSeries 	= new ArrayList<List<Double>>();
		listValueNames 		= new ArrayList<String>();		//List of names for numerical series
		listNumValues 		= new ArrayList<Integer>();
		listTreeSeries 		= new ArrayList<ArrayList<MCMCTree>>();
		listTreeNames 		= new ArrayList<String>();		//List of names for tree series
		listNumTrees 		= new ArrayList<Integer>();
		burnin				= 0;
		indexForC1 = 0;
		indexTreeSeriesForC1 = 0;
		numValueSeries = 0;
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */	
	public void addSerieType(SerieType type) {
		listSerietype.add(type);
		numSeries++;
	}
	
	public void addNewTreeSerie() {
		listTreeSeries.add(new ArrayList<MCMCTree>());
		listNumTrees.add(0);
		numTreeSeries++;
	}
	
	public void addTreeSerie(ArrayList<MCMCTree> listTrees) {
		listTreeSeries.add(listTrees);
		numTreeSeries++;
	}
	
	public void addTreeToSerie(MCMCTree tree, int index) {
		listTreeSeries.get(index).add(tree);
		listNumTrees.set(index, listNumTrees.get(index)+1);
	}
	
	public void addNewValueSerie() {
		listValueSeries.add(new ArrayList<Double>());
		listNumValues.add(0);
		numValueSeries++;
	}
	
	public void addValueSerie(List<Double> listValues) {
		listValueSeries.add(listValues);
		numValueSeries++;
	}
	
	public void addValueToSerie(Double value, int index) {
		listValueSeries.get(index).add(value);
		listNumValues.set(index, listNumValues.get(index)+1);
	}
	
	public void removeSerie(int index) {
		listValueSeries.remove(index);
		listValueNames.remove(index);
		numValueSeries--;
	}
	
	/** getSubDataContainer: Returns part of datacontainer. WARNING: Incomplete */
	public MCMCDataContainer getSubDataContainer(int first, int last) {
		MCMCDataContainer datacontainer = new MCMCDataContainer();
		
		if(first < 0 || last > numLines || last < first)
			return null;
		
		int length = last-first;
		
		datacontainer.setFileName(this.fileName);
		datacontainer.setSerieTypes(this.listSerietype);
		datacontainer.setNumLines(length);
		
		for(int i=0; i<numValueSeries; i++) {
			datacontainer.addValueName(listValueNames.get(i));
			datacontainer.addValueSerie(listValueSeries.get(i).subList(first, last));
		}
		
		return datacontainer;
	}
	
	public int getNumLines() 										{return numLines;}
	public int getNumSeries() 										{return numSeries;}
	public int getNumValues(int index) 								{return listNumValues.get(index);}
	public int getNumValueSeries() 									{return numValueSeries;}	
	public String getFileName() 									{return fileName;}
	public int getNumTrees(int index) 								{return listNumTrees.get(index);}
	public int getNumTreeSeries() 									{return numTreeSeries;}
	public ArrayList<SerieType> getSerieTypes() 					{return listSerietype;}
	public ArrayList<List<Double>> getValueSeries() 				{return listValueSeries;}
	public List<Double> getValueSerie(int index) 					{return listValueSeries.get(index);}
	public ArrayList<ArrayList<MCMCTree>> getTreeSeries() 			{return listTreeSeries;}
	public ArrayList<MCMCTree> getTreeSerie(int index) 				{return listTreeSeries.get(index);}
	public ArrayList<String> getTreeNames() 						{return listTreeNames;}
	public ArrayList<String> getValueNames() 						{return listValueNames;}
	
	public void addTreeName		(String name) 						{listTreeNames.add(name);}
	public void addValueName	(String name) 						{listValueNames.add(name);}
	public void setNumLines		(int numLines)						{this.numLines = numLines;}
	public void setFileName		(String fileName) 					{this.fileName = fileName;}
	public void setSerieTypes	(ArrayList<SerieType> listSerietype){this.listSerietype = listSerietype;}
	public void setNumSeries	(int numSeries) 					{this.numSeries = numSeries;}
	public void setNumTreeSeries(int numTreeSeries) 				{this.numTreeSeries = numTreeSeries;}
	public void setBurnin		(int burnin) 						{this.burnin = burnin;}
	public int getBurnin		()									{return burnin; }
	
	public void setSelectedIndex (int index) { indexForC1 = index; }
	public int getSelectedIndex () { return indexForC1; }
	
	public void setSelectedTreeIndex (int index) { indexTreeSeriesForC1 = index; }
	public int getSelectedTreeIndex () { return indexTreeSeriesForC1; }
	
	public MCMCDataContainer deepCopy() {
		MCMCDataContainer container = new MCMCDataContainer();
		
		container.setFileName(this.fileName);
		container.setSerieTypes(this.listSerietype);
		container.setNumLines(this.numLines);
		
		for(int i=0; i<numValueSeries; i++) {
			container.addValueName(listValueNames.get(i));
			List<Double> temp1 = new ArrayList<Double>();
			for(Double j : listValueSeries.get(i)) 
				temp1.add(j);
			container.addValueSerie(temp1);
		}
		return container;
	}
	
	public MCMCDataContainer deepCopyAndAppendToContainer(MCMCDataContainer container, int start, int end, boolean init) {
		if(init == true) {
			container.setFileName(this.fileName);
			container.setSerieTypes(this.listSerietype);
			container.setNumLines(this.numLines);
		} 
		
		for(int i = 0; i<numValueSeries; i++) {
			container.addValueName(listValueNames.get(i));
			List<Double> temp1 = new ArrayList<Double>();
			for(int j = start; j < end; j++) 
				temp1.add(listValueSeries.get(i).get(j));
			container.addValueSerie(temp1);
		}
		
		return container;
	}
	
	/*
	 * Compute a burnin and assign it to the container (first arg) and return it as well.
	 * 
	 * Notice that the burnin_description is one of
	 * - null, for default choice which is to call calculateLastMaxESSBurnin
	 * - integer, eg "1000"
	 * - double, eg "0.25" 
	 * - string ending in "%", eg "25%" meaning 0.25
	 * - one of geweke (or G), gelmanrubin (or GR), maxess (or ESSM), ness, sess, for automatic burnin decision. 
	 */
		public int calculateBurnin(String burnin_description) throws Exception {
			int burnin = 0;

			if (burnin_description == null || is_burnin_method(burnin_description)) {
				if (getNumLines() < 100) {
					throw new Exception("Less than 100 samples in trace. That is not enough for meaningful burnin estimation.");		
				}
				else {
					if (burnin_description == null || burnin_description.equalsIgnoreCase("maxess") || burnin_description.equalsIgnoreCase("ess")) {
						burnin = calculateLastMethodBurnin(new MaxESSBurnin());
					}
					else if (burnin_description.equalsIgnoreCase("geweke") || burnin_description.equalsIgnoreCase("g")) {
						burnin = calculateLastMethodBurnin(new GewekeBurnin());
					}
					else if (burnin_description.equalsIgnoreCase("gelmanrubin") || burnin_description.equalsIgnoreCase("gr")) {
						burnin = calculateLastMethodBurnin(new GelmanRubinBurnin());
					}
					else if (burnin_description.equalsIgnoreCase("ness")) {
						burnin = MCMCMath.calculateStandardizedESS(this);
					}
					else if (burnin_description.equalsIgnoreCase("sess")) {
						burnin = MCMCMath.calculateNormalizedESS(this);
					}
				}
			}
			else {
				try {
					// Parse an integer, if possible, and set that to burnin. 
					burnin = Integer.parseInt(burnin_description);
					if (burnin > getNumLines() - 1) {
						throw new Exception("Chosen burnin is larger than the size of the trace.");
					}
				}
				catch (NumberFormatException e1) {
					try {
						// If it is not an integer, try a double.
						double burnin_fraction = Double.parseDouble(burnin_description);
						burnin = (int) Math.floor(burnin_fraction * getNumLines());
					}
					catch (NumberFormatException e2) {
						System.err.println("Error! Could not parse the burnin option:" + burnin_description);
						System.exit(-4);
					}
				}
			}

			setBurnin(burnin);
			return burnin;
		}
		
		// This is for using the software pattern "Command"
		private interface BurninCommand {
			public int calculate(Object[] serie);
		}
		private class MaxESSBurnin implements BurninCommand {
			public int calculate(Object[] serie) {
				return MCMCMath.calculateESSmax(serie);
			}
		}
		private class GewekeBurnin implements BurninCommand {
			public int calculate(Object[] serie) {
				return MCMCMath.calculateGeweke(serie);
			}
		}
		private class GelmanRubinBurnin implements BurninCommand {
			public int calculate(Object[] serie) {
				int burnin;
				try {
					burnin = MCMCMath.GelmanRubinBurnin(serie);
				}
				catch (Exception e) {
					burnin = -1;
				}					
				return burnin; 
			}
		}
		
		/*
		 * Calculate burnin for a trace by looking at maximum burnin over all (numerical) parameters in the trace.
		 * One of MaxESSBurnin, GewekeBurnin, or GelmanRubinBurnin is used as the method parameter
		 */
		private int calculateLastMethodBurnin(BurninCommand method) {
			int 						numSeries;
			int 						lastBurnin;
			Object[] 					serie;
			
			lastBurnin = -1;
			
			numSeries = getNumValueSeries();
			if(numSeries != 0) {
				for (List<Double> i : getValueSeries()) {
					serie = i.toArray();

						int burnin = method.calculate(serie);
					if (burnin > lastBurnin) 
						lastBurnin = burnin;
				}
			} 
			if(lastBurnin >= 0) 
				return lastBurnin;
			else
				System.err.println("Warning: no reasonable burnin could be estimated. Using burnin=0 anyways.");
				return 0;
		}
		
		private static boolean is_burnin_method(String s) {
			for (char c: s.toCharArray()) {
				if (Character.isDigit(c)) {
					return false;
				}
			}
			return true;
		}


	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
