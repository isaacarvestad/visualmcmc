package se.cbb.vmcmc.main;

import java.io.*;

public class VersionInfo {
	String version;

	public VersionInfo() {
		version="1.1";
	}
	
	public String getVersion() {
		return version;
	}
}
