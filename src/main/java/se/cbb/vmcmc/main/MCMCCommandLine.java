package se.cbb.vmcmc.main;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import se.cbb.vmcmc.consensustree.ConsensusTree;
import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCFileReader;
import se.cbb.vmcmc.libs.MCMCMath;
import se.cbb.vmcmc.libs.MCMCTree;
import se.cbb.vmcmc.libs.MCMCTreeParamTesting;
import se.cbb.vmcmc.libs.SequenceFileWriter;

/*
 * Execute commands
 * 
 * choice: 4, 5, 6 control how to estimate burnin. 2, 3, 7, 8, 9 are different operations.
 */
public class MCMCCommandLine {
	public static void execute (int choice, MCMCDataContainer datacontainer,
			String burnin_description, double confidencelevel, String path, String outFile, 
			Double alpha, int burnin1) throws Exception {
		/* ******************** FUNCTION VARIABLES ******************************** */
		int 						numSeries;
		int							burnin=0;
		int 						max_ESS;
		int 						geweke;
		int 						overallConvergenceGeweke;
		int 						overallConvergenceMaxESS;
		int 						overallConvergenceGR;
		ArrayList<String> 			numSeriesArray;
		boolean 					gelmanRubin;
		Object[] 					serie;

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		overallConvergenceGeweke = -2;
		overallConvergenceMaxESS = -2;
		overallConvergenceGR = -2;

		/* ******************** FUNCTION BODY ************************************* */
		if (confidencelevel < 0)
			confidencelevel = 0;
		else if(confidencelevel > 100)
			confidencelevel = 100; // Does this even make sense?

		assert choice != 0;
		assert choice != 3; // Moved to separate method call
		assert choice != 8;
		assert choice != 9;
		assert choice != 10;
		assert choice != 11;
		assert choice != 12;
		assert datacontainer != null;

		System.err.println("VMCMC is computing results ... Please wait ...");

		try {
			burnin = datacontainer.calculateBurnin(burnin_description);
		}
		catch (Exception e) {
			System.err.println("Error! " + e.getMessage());
			System.exit(-5);
		}


		numSeries = datacontainer.getNumValueSeries();

		if(numSeries != 0) {
			numSeriesArray = datacontainer.getValueNames();

				System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
				System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
				System.out.println("\t\"Burnin\": " + burnin + ",");
				System.out.println("\t\"Parameters\": {");
			int pos = 0;
			for (List<Double> valueSerie : datacontainer.getValueSeries()) {
				serie = valueSerie.toArray();

				if(serie.length < 100){
					System.out.println("\t\tSmall dataset. Statistics and tests can not be computed.\n\t}\n}");
					System.exit(0);
				}

				System.out.println("\t\t\"" + numSeriesArray.get(pos) + "\": " + "{");

				if(choice == 5) {
					max_ESS = MCMCMath.calculateESSmax(serie);
					System.out.println("\t\t\t\"maxESS\": {\n\t\t\t\t\"Status\": \"Converged\",");
					System.out.println("\t\t\t\t\"Burn_in\": " + max_ESS + "\n\t\t\t}");
				} else if(choice == 4) {
					geweke = MCMCMath.calculateGeweke(serie);
					if (geweke != -1) {
						System.out.println("\t\t\t\"Geweke\": {\n\t\t\t\t\"Status\": \"Converged\",");
						System.out.println("\t\t\t\t\"Burn_in\": " + geweke + "\n\t\t\t}");							
					} else 
						System.out.println("\t\t\t\"Geweke\": \"Not converged\"");
				} else if(choice == 6) {
					gelmanRubin = MCMCMath.GelmanRubinTest(serie, burnin);
					if(gelmanRubin == true)	{
						System.out.println("\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \"Converged\",");
					} else {
						System.out.println("\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \"Not Converged\",");
					}
				} 

				if(choice == 2 || choice == 7) {
					geweke = MCMCMath.calculateGeweke(serie);
					max_ESS = MCMCMath.calculateESSmax(serie);

					System.out.println("\t\t\t\"maxESS\": {\n\t\t\t\t\"Status\": \"Converged\",");
					System.out.println("\t\t\t\t\"Burn_in\": " + max_ESS + "\n\t\t\t},");
					if(max_ESS > overallConvergenceMaxESS) {
						overallConvergenceMaxESS = max_ESS;
					}

					if (geweke != -1) {
						System.out.println("\t\t\t\"Geweke\": {\n\t\t\t\t\"Status\": \"Converged\",");
						System.out.println("\t\t\t\t\"Burn_in\": " + geweke + "\n\t\t\t},");	
						if(overallConvergenceGeweke != -1 && geweke > overallConvergenceGeweke) {
							overallConvergenceGeweke = geweke;
						}
					} else {
						System.out.println("\t\t\t\"Geweke\": \"Not converged\"");
						overallConvergenceGeweke = -1;
					}

					int originalBurnin = 0;
					gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
					while(gelmanRubin != true && originalBurnin < (0.5 * serie.length)) {
						originalBurnin = originalBurnin + 1;
						gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
					}
					if(gelmanRubin == true)	{
						System.out.println("\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \"Converged\",");
						if(choice != 7)
							System.out.println("\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t}");
						else
							System.out.println("\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t},");
						if(overallConvergenceGR != -1 && originalBurnin > overallConvergenceGR)
							overallConvergenceGR = originalBurnin;
					} else {
						System.out.println("\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \" Not Converged\",");
						if(choice != 7)
							System.out.println("\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t}");
						else
							System.out.println("\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t},");
						overallConvergenceGR = -1;
					}
				}

				if(serie.length - burnin > 0 && (choice == 7)) {
					ComputeAndPrintStatistics(burnin, serie, confidencelevel);
				}

				System.out.println("\t\t}");
		
				if(pos < numSeries - 1 && choice < 8)
					System.out.println(",");
				pos++;
			}
		} 
	}
		/* ******************** END OF FUNCTION *********************************** */


	/**
	 * Decide on a burnin using ESS and then print a sample in JSON format
	 * 
	 * @param file1
	 * @param burnin
	 * @param path
	 * @param outFile
	 * @param numSeries
	 * @param serieLength
	 * @param datacontainer
	 * @throws Exception
	 */
	public static void pickAndWriteTraceSample(MCMCDataContainer datacontainer, int burnin)
					throws Exception {
		ArrayList<String> numSeriesArray;

		assert burnin >= 0;
		assert datacontainer != null;
		
		System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
		System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
		System.out.println("\t\"Burnin\": " + burnin + ",");

		int chosenElement = burnin + (int)(Math.random() * (datacontainer.getNumLines()- burnin + 1));
		System.out.println("\t\"Sample\": " + chosenElement + ",");
		System.out.println("\t\"Parameters\": {");
		numSeriesArray = datacontainer.getValueNames();
		ArrayList<String> treeSeriesNames = datacontainer.getTreeNames();

		int pos = 0;
		for (List<Double> i : datacontainer.getValueSeries()) {
			System.out.println("\t\t\"" + numSeriesArray.get(pos) + "\" : " + i.get(chosenElement - 1));
			if(pos < datacontainer.getNumSeries() - 1)
				System.out.println(",");
			pos++;
		}
		if(treeSeriesNames.size() != 0)
			System.out.println(",");
		System.out.println("");

		pos = 0;
		for (ArrayList<MCMCTree> i : datacontainer.getTreeSeries()) {
			String newtree = new String(i.get(chosenElement - 1).getData());
			System.out.println("\t\t\"" + treeSeriesNames.get(pos) + "\" : \"" + newtree + "\"");
			if(pos < treeSeriesNames.size() -1)
				System.out.println(",");
			pos++;
		}
		System.out.println("\t}");
	}


	public static void printConvergenceTestResults(MCMCDataContainer datacontainer) {
		try {
			int standardizedESS = MCMCMath.calculateStandardizedESS(datacontainer);
			int normalizedESS = MCMCMath.calculateNormalizedESS(datacontainer);					
			int overallConvergenceGeweke = datacontainer.calculateBurnin("geweke");
			int overallConvergenceESS = datacontainer.calculateBurnin("maxess");
			int overallConvergenceGR = datacontainer.calculateBurnin("gelmanrubin");


			System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
			System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
			
			System.out.println("\t\"Convergence_Test_Results\": {");
			if(overallConvergenceGeweke == -1) 
				System.out.println("\t\t\"Geweke\": \"All parameters did not converge.\",");
			else 
				System.out.println("\t\t\"Geweke\": " + overallConvergenceGeweke + ",");
			if(overallConvergenceESS == -1) 
				System.out.println("\t\t\"MaxESS\": \"All parameters did not converge.\",");
			else 
				System.out.println("\t\t\"MaxESS\": " + overallConvergenceESS + ",");
			if(overallConvergenceGR == -1) 
				System.out.println("\t\t\"Gelman_Rubin\": \"All parameters did not converge.\",");
			else
				System.out.println("\t\t\"Gelman_Rubin\": " + overallConvergenceGR + ",");
			System.out.println("\t\t\"Standardized ESS\": " + standardizedESS + ",");
			System.out.println("\t\t\"Normalized ESS\": " + normalizedESS);
			System.out.println("\t}");
			System.out.println("}"); 
		}
		catch (Exception e) {
			System.err.println("Error! " + e.getMessage());
			System.exit(-6);
		}
	}
	
	public static void printTreeConvergenceTest(MCMCDataContainer datacontainer, int burnin, double alpha) {
		System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
		System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
		System.out.println("\t\"Burnin\": " + burnin + ",");
		
		parallelConvergenceTest(alpha, datacontainer, datacontainer, burnin, (burnin + datacontainer.getValueSerie(0).size())/2);
		
		System.out.println("}");
	}
	
	private static void parallelConvergenceTest(Double alpha, MCMCDataContainer datacontainer1, MCMCDataContainer datacontainer2, int burnin1, int burnin2) {
		ArrayList<String> series1 = new ArrayList<String>();
		ArrayList<String> series2 = new ArrayList<String>();

		// TODO: It is silently assumed that there is only one tree parameter in the trace. This might be an issue in the future.
		for(int i = burnin1; i < datacontainer1.getTreeSerie(datacontainer1.getSelectedTreeIndex()).size(); i++)
			series1.add(new String(datacontainer1.getTreeSerie(datacontainer1.getSelectedTreeIndex()).get(i).getData()));
		for(int i = burnin2; i < datacontainer2.getTreeSerie(datacontainer2.getSelectedTreeIndex()).size(); i++)
			series2.add(new String(datacontainer2.getTreeSerie(datacontainer2.getSelectedTreeIndex()).get(i).getData()));
		try {
			ArrayList<Double> res = MCMCTreeParamTesting.performTest(series1, series2, 0, 0, alpha, true);		
			System.out.print("\t\"Test statistic\" : " + res.get(0) + ",\n\t\"p-value\" : " + res.get(1) + ",\n\t\"Same distribution\" : ");
			if(res.get(1) < alpha)
				System.out.println("false");
			else
				System.out.println("true");
		}
		catch (Exception e) {
			System.err.println("Error! " + e.getMessage());
			System.exit(-7);

		}
	}

	
	public static void printLine(String path, String outFile, String str) throws Exception {
		if(outFile.equals("stdout")) 
			System.out.println(str);
		else
			SequenceFileWriter.writeAndAppendLine(path, outFile, str);
	}
	
	public static void ComputeAndPrintStatistics(int burnin, Object[] serie, double confidencelevel) throws Exception {
		Double[] data;
		
		int 						numValues;
		int 						equalStart; 
		int 						equalEnd;
		int 						tempHolder;
		int 						intervalLength;
		int 						startPos; 
		int 						leftIndex;
		int 						rightIndex;
		double 						values;
		double 						values1;
		double 						mean;
		double 						sigmaSquare;
		double					 	arithmetic_standard_dev;
		double 						power;
		double 						geometric_mean;
		double 						sum;
		double 						sum1;
		double 						geometric_standard_dev;
		double 						harmonic_mean;
		double		 				max_value;
		double				 		min_value;
		double 						comp;
		double 						nearest;
		double				 		startNum;
		double 						tempHolder1;
		double 						tempHolder2;

		data = new Double[serie.length-burnin];
		System.arraycopy(serie, burnin, data, 0, serie.length-burnin);

		numValues = data.length;
		values = 0;
		values1	= 1;
		sigmaSquare = 0;
		power = (double) 1/numValues; 
		sum = 0;
		sum1 = 0;
		max_value = data[0];
		min_value = data[0];

		for(Double k : data) {
			values += k;
			values1	*= java.lang.Math.pow(k, power);
			sum += Math.pow(Math.log(k), 2);
			sum1 += (double)1/k;

			if(k > max_value)
				max_value = k;

			if(k < min_value)
				min_value = k;
		}
		mean = values/numValues;
		geometric_mean = values1;
		harmonic_mean = (double)numValues/sum1;

		for(Double k : data)
			sigmaSquare += java.lang.Math.pow(k - mean,2);

		arithmetic_standard_dev = (double)java.lang.Math.sqrt(sigmaSquare/(numValues-1));
		geometric_standard_dev = Math.abs(Math.exp(Math.sqrt(sum/(numValues-1) - ((numValues/(numValues-1))*Math.pow(Math.log(values1),2)))));

		System.out.println("\t\t\t\"Arithmetic Mean\": " + mean + ",");
		System.out.println("\t\t\t\"Arithmetic Standard Deviation\": " + arithmetic_standard_dev + ",");
		if(!Double.isNaN(geometric_mean))
			System.out.println("\t\t\t\"Geometric Mean\": " + geometric_mean + ",");
		else
			System.out.println("\t\t\t\"Geometric Mean\": \"" + geometric_mean + "\",");
		if(!Double.isNaN(geometric_standard_dev))
			System.out.println("\t\t\t\"Geometric Standard Deviation\": " + geometric_standard_dev + ",");
		else
			System.out.println("\t\t\t\"Geometric Standard Deviation\": \"" + geometric_standard_dev + "\",");
		System.out.println("\t\t\t\"Harmonic Mean\": " + harmonic_mean + ",");
		System.out.println("\t\t\t\"Minimum Value\": " + min_value + ",");
		System.out.println("\t\t\t\"Maximum Value\": " + max_value + ",");

		Arrays.sort(data);
		nearest	=(Double)data[0];
		equalStart = 0;
		equalEnd = 0;

		for(int k = 0; k < numValues-1; k++) {
			comp = Math.abs(mean-nearest) - Math.abs(mean-(Double)data[k+1]);
			if (comp > 0) {
				nearest = (Double)data[k+1]; 
				equalStart = k+1;
			} else if (comp == 0) 
				equalEnd = k+1;
		}

		if(equalEnd == 0)
			tempHolder = equalStart;
		else
			tempHolder = equalStart + (equalEnd - equalStart)/2;

		double[] start = {nearest, tempHolder};
		intervalLength = (int) ((double)(numValues)*(confidencelevel/100));
		startPos = (int)start[1]; 
		startNum = start[0];
		leftIndex = startPos-1;
		rightIndex = startPos+1;

		System.out.println("\t\t\t\"Confidence_Level\": " + confidencelevel + ",");

		if(numValues == 0) {
			tempHolder1 = Double.NaN;
			tempHolder2 = Double.NaN;
			double[] result = {tempHolder1, tempHolder2};
			System.out.println("\t\t\t\"Credible Interval\": \"" + result[0] + " ; " + result[1] + "\"");
		} else if(numValues == 1) {
			tempHolder1 = (Double) data[0];
			tempHolder2 = (Double) data[0];
			double[] result = {tempHolder1, tempHolder2};
			System.out.println("\t\t\t\"Credible Interval\": \"" + result[0] + " ; " + result[1] + "\"");
		} else if(numValues == 2) {
			tempHolder1 = (Double) data[0];
			tempHolder2 = (Double) data[1];
			double[] result = {tempHolder1, tempHolder2};
			System.out.println("\t\t\t\"Credible Interval\":  \"" + result[0] + " ; " + result[1] + "\"");
		} else {
			for(int k = 0 ; k < intervalLength ; k++) {
				if(leftIndex == 0)
					if(rightIndex < numValues-1)
						rightIndex++;
				if(rightIndex == numValues-1)
					if(leftIndex > 0)
						leftIndex--;
				if(leftIndex > 0 && Math.abs((Double)data[leftIndex] - startNum) <= Math.abs((Double)data[rightIndex] - startNum))
					leftIndex--;
				else if(rightIndex < numValues-1 && Math.abs((Double)data[leftIndex] - startNum) > Math.abs((Double)data[rightIndex] - startNum))
					rightIndex++;
			}
			double[] result = {(Double) data[leftIndex], (Double) data[rightIndex]};
			System.out.println("\t\t\t\"Credible Interval\": \"" + result[0] + " ; " + result[1] + "\"");
		}
	}
	
	public static void parallelChainAnalysis(int burnin1, int burnin2, Double alpha,  String filename1, String filename2) throws Exception {
		File file1 = new File(filename1);
		File file2 = new File(filename2);
		final NumberFormat formatter = new DecimalFormat("0.00");
		
		MCMCDataContainer datacontainer1 = MCMCFileReader.readMCMCFile(file1, false, false);
		MCMCDataContainer datacontainer2 = MCMCFileReader.readMCMCFile(file2, false, false);
				
		burnin1 = datacontainer1.calculateBurnin(null); // Not good that this is null. Should be a description string: params.burnin1
		burnin2 = datacontainer2.calculateBurnin(null);
					
		System.out.println("{\n\t\"File1\": \"" + file1.getAbsolutePath() + "\",");
		System.out.println("\t\"# Samples in file1\": \"" + datacontainer1.getNumLines() + "\",");
		System.out.println("\t\"Burnin for file1\": \"" + burnin1 + "\",");
		System.out.println("\t\"File2\": \"" + file2.getAbsolutePath() + "\",");
		System.out.println("\t\"# Samples in file2\": \"" + datacontainer2.getNumLines() + "\",");
		System.out.println("\t\"Burnin for file2\": \"" + burnin2 + "\",");
		System.out.println("\t\"Alpha\": \"" + alpha + "\",");
		
		System.out.println("\t\"Parameters\": {");
		
		for(int i = 0; i < datacontainer1.getValueSeries().size(); i++) {
			System.out.println("\t\t\"" + datacontainer1.getValueNames().get(i) + "\": {");
			int numPoints1 			= datacontainer1.getValueSerie(i).size();	
			int numBurnInPoints1 	= burnin1;	
			int numPoints2 			= datacontainer2.getValueSerie(i).size();
			int numBurnInPoints2 	= burnin2;
			
			final double[] sample1 	= new double[numPoints1 - numBurnInPoints1];
			final double[] sample2 	= new double[numPoints2 - numBurnInPoints2];
			List<Double> temp1 = datacontainer1.getValueSerie(i);
			List<Double> temp2 = datacontainer2.getValueSerie(i);
			
			for(int j = 0 ; j < temp1.size() - numBurnInPoints1; j++) 
				sample1[j] = temp1.get(j + numBurnInPoints1);
			for(int j=0 ; j < temp2.size() - numBurnInPoints2; j++) 
				sample2[j] = temp2.get(j + numBurnInPoints2);

			MannWhitneyUTest mw  = new MannWhitneyUTest();
			double testStatistic = mw.mannWhitneyU(sample1,sample2);
			double pValue  = mw.mannWhitneyUTest(sample1,sample2);

			System.out.println("\t\t\t\"Test statistic\" : " + testStatistic + ",");
			System.out.println("\t\t\t\"p-value\" : " + formatter.format(pValue) + ",");
			if(pValue > alpha )
				System.out.println("\t\t\t\"Decision\" : \"Same distr.\"\n\t\t},");
			else
				System.out.println("\t\t\t\"Decision\" : \"Different distr.\"\n\t\t},");
		
			double min1 = sample1[0];
			double max1 = sample1[0];
			for(int j = 1; j < sample1.length; j++) {
				if(sample1[j] < min1)
					min1 = sample1[j];
				if(sample1[j] > max1)
					max1 = sample1[j];
			}
			
			for(int j = 0; j < sample2.length; j++) {
				if(sample2[j] < min1)
					min1 = sample2[j];
				if(sample2[j] > max1)
					max1 = sample2[j];
			}
			
			System.out.println("MinMax : " + min1 + "\t" + max1);
			
			int numberofbins = (sample2.length / 100) + 1;
			
			double[] binValues = new double[numberofbins];
			for(int j = 0; j < numberofbins; j++) {
				binValues[j] = min1 + (j * ((max1-min1)/(numberofbins-1)));
			}
			
			long[] s1 = new long[numberofbins];
			long[] s2 = new long[numberofbins];
			
			for(int j = 0; j < numberofbins; j++) {
				s1[j] = 0;
				s2[j] = 0;
			}
			
			for(int j = 0; j < sample1.length; j++) {
				for (int k = 1; k < numberofbins; k++) {
					if(sample1[j] >= binValues[k-1] && sample1[j] < binValues[k]) {
						s1[k] = s1[k] + 1;
						break;
					}
				}
			}
			
			for(int j = 0; j < sample2.length; j++) {
				for (int k = 1; k < numberofbins; k++) {
					if(sample2[j] >= binValues[k-1] && sample2[j] < binValues[k]) {
						s2[k] = s2[k] + 1;
						break;
					}
				}
			}
			
			long sum1 = 0;
			long sum2 = 0;
			for(int j = 0; j < binValues.length; j++) {
//				System.out.println(binValues[j] + "\t" + s1[j] + "\t" + s2[j]);
				sum1 = sum1 + s1[j];
				sum2 = sum2 + s2[j];
			}
			
			s1[numberofbins - 1] = s1[numberofbins - 1] + sample1.length - sum1;
			s2[numberofbins - 1] = s2[numberofbins - 1] + sample2.length - sum2;
			
			ArrayList<Long> list1 = new ArrayList<Long>();
			ArrayList<Long> list2 = new ArrayList<Long>();
			
			for(int j = 0; j < numberofbins; j++) {
				if(s1[j] > 0 && s2[j] > 0 && s1[j] > 50 && s2[j] > 50 ) {
					list1.add(s1[j]);
					list2.add(s2[j]);
				}
			}
			
			sum1 = 0;
			sum2 = 0;
			
			for(int j = 0; j < binValues.length; j++) {
				System.out.println(binValues[j] + "\t" + s1[j] + "\t" + s2[j]);
				sum1 = sum1 + s1[j];
				sum2 = sum2 + s2[j];
			}
			
			System.out.println("Net sum : " + sum1 + "\t" + sum2);
			
			sum1 = 0;
			sum2 = 0;
			
/*			for(int j = 0; j < list1.size(); j++) {
				System.out.println(list1.get(j) + "\t" + list2.get(j));
				sum1 = sum1 + list1.get(j);
				sum2 = sum2 + list2.get(j);
			}*/
			
			assert(list1.size() == list2.size());
			
			long [] data1 = new long[list1.size()];
			long [] data2 = new long[list2.size()];
					
			for(int j = 0; j < list1.size() ; j++ ) {
				data1[j] = list1.get(j).longValue();
				data2[j] = list2.get(j).longValue();
			}
			
			for(int j = 0; j < data1.length; j++) {
				System.out.println("Data1 Entry " + j + "\t\t" + data1[j] + "\t" + data2[j]);
				sum1 = sum1 + data1[j];
				sum2 = sum2 + data2[j];
			}
			
			assert(data1.length == data2.length);
			
			System.out.println("data1 Length = " + data1.length + " Data2 Length = " + data2.length);
			
			if(data1.length > 1) {
				ChiSquareTest chi  = new ChiSquareTest();

				ArrayList<String> res = new ArrayList<String>();
				res.add(String.valueOf(chi.chiSquareDataSetsComparison(data1,data2)));
				res.add(String.valueOf(chi.chiSquareTestDataSetsComparison(data1,data2)));

				System.out.println("Net sum : " + sum1 + "\t" + sum2);

				System.out.println("------------------------------------");

				if(Double.parseDouble(res.get(1)) < alpha)
					System.out.println("\t\t\t\"Test statistic\" : " + res.get(0) + ",\n\t\t\t\"pValue\" : " + res.get(1) + ",\n\t\t\t\"Decision\" : Different distribution\n\t\t},");
				else
					System.out.println("\t\t\t\"Test statistic\" : " + res.get(0) + ",\n\t\t\t\"pValue\" : " + res.get(1) + ",\n\t\t\t\"Decision\" : Same distribution\n\t\t},");
			} else if (data1.length == 1) {
				System.out.println("Net sum : " + sum1 + "\t" + sum2);

				System.out.println("------------------------------------");
				System.out.println("\t\t\t\"Test statistic\" : " + 0 + ",\n\t\t\t\"pValue\" : " + 1 + ",\n\t\t\t\"Decision\" : Same distribution\n\t\t},");
			}
		}
		
		System.out.println("\t\t\"Tree topology" + "\": {");
		
		MCMCCommandLine.parallelConvergenceTest(alpha, datacontainer1, datacontainer2, burnin1, burnin2);
	}
	
	public static void printTreePosterior(MCMCDataContainer datacontainer, int burnin) throws Exception {
		int 						numPoints;
		int 						key;
		int 						numDuplicates;
		TreeMap<Integer, MCMCTree> 	treeMap;
		ArrayList<MCMCTree> 		uniqueserie;
		MCMCTree 					uniqueTree;
		ArrayList<TreeMap<Integer, MCMCTree>> 	treeMapList;
		MCMCTree[] 					treeData;
		treeMapList	= new ArrayList<TreeMap<Integer, MCMCTree>>();
		final NumberFormat 			formatter = new DecimalFormat("0.00");

		if (datacontainer.getNumTreeSeries() < 1) 
			throw new Exception("No tree data found in the trace.");

		int pos = 0;
		for(ArrayList<MCMCTree> treeSerie : datacontainer.getTreeSeries()) {
			treeMap = new TreeMap<Integer, MCMCTree>();
			treeMapList.add(treeMap);

			numPoints 				= treeSerie.size();	
			treeData 				= new MCMCTree[numPoints-burnin];

			System.arraycopy(treeSerie.toArray(), burnin, treeData, 0, numPoints-burnin);

			for(MCMCTree tree : treeData) {
				key = tree.getKey();
				uniqueTree = treeMap.get(key);
				if(uniqueTree == null) {
					uniqueTree = new MCMCTree();

					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
					uniqueTree.setData(tree.getData());
					treeMap.put(key, uniqueTree);
				} else {
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
				}
			}
			treeMap = treeMapList.get(pos);
			uniqueserie	= new java.util.ArrayList<MCMCTree>(treeMap.values());

			Collections.sort(uniqueserie);

			if(pos == 0) {
				System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
				System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
				System.out.println("\t\"Burnin\": " + burnin + ",");
				System.out.println("\t\"Trees\": {");
			}
			System.out.println("\t\t\"Series_" + pos + "\": [");

			int pos1 = 0;
			for(MCMCTree t : uniqueserie) {
				System.out.println("\t\t\t{");
				String newick = new String(t.getData());
				numDuplicates = t.getNumDuplicates();
				System.out.println("\t\t\t\t\"Index\": " + pos1 + ",\n\t\t\t\t\"Duplicates\": " + numDuplicates + ",\n\t\t\t\t\"Posterior probability\": " + (formatter.format((double) numDuplicates/(numPoints-burnin))) + ",\n\t\t\t\t\"Newick\": \"" + newick + "\"" + "\n\t\t\t}");
				if(pos1 < uniqueserie.size()-1)
					System.out.println(",");
				pos1++;
			}
			System.out.println("\n\t\t]");
			if(pos == 0 && datacontainer.getNumTreeSeries() > 1) 
				System.out.println(",");
			else 
				System.out.println("");
			pos++;
		}
		System.out.println("\t}\n}");
	}
	
	public static void determineMaximumAPosterioriTree(MCMCDataContainer datacontainer, int burnin) throws Exception {
		int 						numPoints;	
		int 						numBurnInPoints;
		int 						key;
		TreeMap<Integer, MCMCTree> 	treeMap;
		ArrayList<MCMCTree> 		uniqueserie;
		MCMCTree 					uniqueTree;
		ArrayList<TreeMap<Integer, MCMCTree>> 	treeMapList;
		ArrayList<MCMCTree> 		treeSerie;
		MCMCTree[] 					treeData;
		final NumberFormat 			formatter = new DecimalFormat("0.00");

		if (datacontainer.getNumTreeSeries() < 1) 
				throw new Exception("No tree data found in the trace.");
		
		ArrayList<String> treeSeriesNames = datacontainer.getTreeNames();

		treeMapList	= new ArrayList<TreeMap<Integer, MCMCTree>>();
		for(int i = 0; i < datacontainer.getNumTreeSeries(); i++) {
			treeMap = new TreeMap<Integer, MCMCTree>();
			treeMapList.add(treeMap);

			treeSerie = datacontainer.getTreeSerie(i);
			numPoints = treeSerie.size();	
			numBurnInPoints	= burnin;
			treeData = new MCMCTree[numPoints-numBurnInPoints];

			System.arraycopy(treeSerie.toArray(), numBurnInPoints, treeData, 0, numPoints-numBurnInPoints);

			for(MCMCTree tree : treeData) {
				key = tree.getKey();
				uniqueTree = treeMap.get(key);

				if(uniqueTree == null) {
					uniqueTree = new MCMCTree();

					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
					uniqueTree.setData(tree.getData());
					treeMap.put(key, uniqueTree);
				} else {
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
				}
			}
			treeMap = treeMapList.get(i);
			uniqueserie	= new java.util.ArrayList<MCMCTree>(treeMap.values());

			Collections.sort(uniqueserie);

			uniqueTree = uniqueserie.get(0);

			String newickseq = new String(uniqueTree.getData());
			Integer duplicates = uniqueTree.getNumDuplicates();
			for(MCMCTree j : uniqueserie) {
				if(j.getNumDuplicates() > duplicates) {
					newickseq = new String(j.getData());
					duplicates = j.getNumDuplicates();
				}
			}
			if (i == 0) {
				System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
				System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
				System.out.println("\t\"Burnin\": " + burnin + ",");
				System.out.println("\t\"MAP_trees\" : [");
			}
			System.out.println("\t\t\"" + treeSeriesNames.get(i) + "\" : {");
			System.out.println("\t\t\t\"Index\": " + i + ",");
			System.out.println("\t\t\t\"MAP_Probability\": " + (formatter.format((double) duplicates/(numPoints-numBurnInPoints))) + ",");
			System.out.println("\t\t\t\"Tree\": \"" + newickseq + "\"");
			if(i == 0 && datacontainer.getNumTreeSeries() > 1) 
				System.out.println("\t\t},");
			else 
				System.out.println("\t\t}");
		}
		System.out.println("\t]\n}");

	}

	public static void determineConsensusTrees(MCMCDataContainer datacontainer, int burnin) throws Exception {
		assert datacontainer != null;
		assert burnin > -1;

		if (datacontainer.getNumTreeSeries() < 1) 
			throw new Exception("No tree data found in the trace.");
		
		ArrayList<String> treeSeriesNames = datacontainer.getTreeNames();
		
		System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
		System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
		System.out.println("\t\"Burnin\": " + burnin + ",");
		System.out.println("\t\"Consensus trees\" : [");

		int pos = 0;
		for(ArrayList<MCMCTree> treeSerie : datacontainer.getTreeSeries()) {
			TreeMap<Integer, MCMCTree> posterior = estimateTreePosterior(treeSerie, burnin);

			ArrayList<MCMCTree> trees = new ArrayList<MCMCTree>(posterior.values());
			ArrayList<Integer> freqs = new ArrayList<Integer>(posterior.keySet());
			
			ArrayList<String> newickTrees = new ArrayList<String>();
			for (MCMCTree t: trees) {
				newickTrees.add(new String(t.getData()));
			}
			String newick_consensus = ConsensusTree.consensustree(newickTrees, freqs);
			System.out.print("\t\t\"" + treeSeriesNames.get(pos) + "\" : " + newick_consensus);
			if (pos < datacontainer.getNumTreeSeries() - 1) {
				System.out.println(",");
			}
			else {
				System.out.println("\n\t]");
			}
			pos++;
		}
		System.out.println("}");
	}

	/*
	 * Input: a trace in a datacontainer and a burnin
	 * Ouput: A map containing, essentially, the information tree index, tree, duplicity, and estimated probability
	 */
	private static TreeMap<Integer, MCMCTree> estimateTreePosterior(ArrayList<MCMCTree> treeSerie, int burnin) {
		TreeMap<Integer, MCMCTree> treeMap = new TreeMap<Integer, MCMCTree>();
//		treeMapList.add(treeMap);

//		MCMCTree[] treeData = new MCMCTree[treeSerie.size()-burnin];
//
//		System.arraycopy(treeSerie.toArray(), burnin, treeData, 0, treeSerie.size()-burnin);

//		for(MCMCTree tree : treeSerie) {
		for (int i=burnin; i<treeSerie.size(); i++) {
			MCMCTree tree = treeSerie.get(i);
			int key = tree.getKey();
			MCMCTree uniqueTree = treeMap.get(key);
			if(uniqueTree == null) {
				uniqueTree = new MCMCTree();
				uniqueTree.setData(tree.getData());
				uniqueTree.addIndex(tree.getIndexList().get(0));
				uniqueTree.addDuplicate();
				treeMap.put(key, uniqueTree);
			} else {
				uniqueTree.addIndex(tree.getIndexList().get(0));
				uniqueTree.addDuplicate();
			}
		}
		return treeMap;
	}
	

	
	public static void printSimpleStats(MCMCDataContainer datacontainer, 
			int burnin, double confidencelevel,
			Double alpha, int burnin1) throws Exception {

		Object[] 					serie;
		ArrayList<String> 			serieNames;

		assert burnin >= 0;
		
		try {
			int pos = 0;
			serieNames = datacontainer.getValueNames();
			
			System.out.println("{\n\t\"File\": \"" + datacontainer.getFileName() + "\",");
			System.out.println("\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
			System.out.println("\t\"Burnin\": " + burnin + ",");
			System.out.println("\t\"Parameters\": {");


			for (List<Double> valueSerie : datacontainer.getValueSeries()) {
				serie = valueSerie.toArray();

				if(serie.length < 100){
					System.err.println("\t\tSmall dataset. Statistics and tests can not be computed.\n\t}\n}");
					System.exit(0);
				}

				System.out.println("\t\t\"" + serieNames.get(pos) + "\": " + "{");

				if (serie.length - burnin > 0) {
					ComputeAndPrintStatistics(burnin, serie, confidencelevel);
				}
				System.out.println("\t\t}");

				if (pos < datacontainer.getNumValueSeries() - 1)
					System.out.println(",");

				pos++;
			}
		}
		catch (Exception e) {
			System.err.println("Error! Could not read the trace file.");
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
			System.exit(-2);
		}
	}
}
