package se.cbb.vmcmc.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import se.cbb.vmcmc.gui.MCMCMainTab;
import se.cbb.vmcmc.gui.MCMCOverviewTab;
import se.cbb.vmcmc.gui.MCMCParallelMainTab;
import se.cbb.vmcmc.gui.MCMCProgressBar;
import se.cbb.vmcmc.gui.MCMCTableTab;
import se.cbb.vmcmc.gui.MCMCTreeAnalysisTab;
import se.cbb.vmcmc.gui.MCMCTreeParallelTab;
import se.cbb.vmcmc.gui.MCMCTreeTab;
import se.cbb.vmcmc.gui.MCMCWindow;
import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCFileReader;
import se.cbb.vmcmc.libs.MCMCMath;
import se.cbb.vmcmc.libs.MCMCNumericFunctions;
import se.cbb.vmcmc.libs.MCMCTreeFunctions;

public class MCMCFileAnalyser {
	private static ArrayList<Integer> essMax;
	public static void fileOpener(File file, MCMCWindow window, boolean runTime, boolean parallel) {
		String decoded 							= "";
		int seriesID 							= -1;
		MCMCDataContainer datacontainer 		= null;
		MCMCMainTab mainPanel 					= null;
		MCMCTableTab tablePanel 				= null;
		MCMCTreeTab treePanel 					= null;
		MCMCTreeAnalysisTab treeAnalysisPanel 	= null;
		MCMCOverviewTab overviewPanel           = null;
		JTabbedPane tabs 						= new JTabbedPane();
		int overallConvergenceESS 				= -2;
		int burnin;
		JFrame frame;
		
		if(parallel == false)
			frame = new JFrame("VMCMC Progress Bar");
		else
			frame = new JFrame("VMCMC Parallel Progress");
		frame.setBackground(new Color(0xFFEEEEFF));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        MCMCProgressBar progressBar;
        if(runTime == false) {
        	progressBar = new MCMCProgressBar(0);
        	progressBar.setProgress(0, "Finding file");
        } else {
        	progressBar = new MCMCProgressBar(45);
        	progressBar.setProgress(45, "Finding file");
        }
        
        JComponent newContentPane = progressBar;
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
        frame.update(frame.getGraphics());
        frame.setVisible(true);
        frame.validate();
        window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if(file != null) {						
			try {
				//Call library method to store contents of file to MCMCDataContainer
				datacontainer = MCMCFileReader.readMCMCFile(file, true, false);
				if(parallel == true && runTime == false)
					progressBar.setProgress(25, "loading file");
				else
					progressBar.setProgress(50, "loading file");
			} catch(IOException e) {
				JOptionPane.showMessageDialog(new JFrame(), "Input Error. Verify that the file exists and the correct path is provided.");
				return;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(new JFrame(), "Invalid MCMC file format. Currently MCMC runs from PrIMe, JPrIMe, BEAST and MrBayes are supported. \nSee examples on https://code.google.com/p/vmcmc/ for further directions.");
				return;
			}
			
			tabs.setBackground(new Color(0xFFDDDDFF));
			tabs.setFocusable(false);
			tabs.setBorder(BorderFactory.createLineBorder(new Color(0xFFEEEEFF), 2));

			//If there are numerical values in file. Create main panel and table panel
			if(datacontainer.getNumValueSeries() != 0) {
				overviewPanel = MCMCNumericFunctions.createOverviewPanel(datacontainer, window);
				
				if(parallel == true && runTime == false)
					progressBar.setProgress(40, "computing overview panel");
				else
					progressBar.setProgress(55, "computing overview panel");
				
				mainPanel = MCMCNumericFunctions.createMainPanel(datacontainer, window);
				
				if(parallel == true && runTime == false)
					progressBar.setProgress(42, "computing graph panel");
				else
					progressBar.setProgress(62, "computing graph panel");
				
				essMax = MCMCNumericFunctions.getEssMax();
				tablePanel = MCMCNumericFunctions.createTablePanel(datacontainer);
				
				if(parallel == true && runTime == false)
					progressBar.setProgress(45, "loading table panel");
				else
					progressBar.setProgress(65, "loading table panel");
				
				MCMCNumericFunctions.linkOverviewToMain(overviewPanel);

				tabs.add("Overview", overviewPanel);
				tabs.add("Graph", mainPanel);
				tabs.add("Table", tablePanel);
			}

			//If there are tree parameters in file. Create tree tab.
			if(datacontainer.getNumTreeSeries() != 0) {
				if(datacontainer.getNumValueSeries() != 0) {
					overallConvergenceESS = essMax.get(0);
					for (Integer i : essMax) {
						if(i == -1) {
							overallConvergenceESS = -1;
							break;
						} else if(overallConvergenceESS < i)
							overallConvergenceESS = i;
					}
				} 
				
				for(int i = 0; i < datacontainer.getNumTreeSeries(); i++) {
					try {
						decoded = new String((datacontainer.getTreeSerie(i).get(0).getData()), "UTF-8");
					} catch (UnsupportedEncodingException e) {
					}
					if(decoded.indexOf(":") == -1)
						seriesID = i;
				}
				
				if(overallConvergenceESS == -1) 
					burnin = 0;
				else
					burnin = overallConvergenceESS;
				datacontainer.setBurnin(burnin);
				
				treePanel = MCMCTreeFunctions.createTreePanel(datacontainer, window);
				
				if(parallel == true && runTime == false)
					progressBar.setProgress(47, "loading tree panel");
				else
					progressBar.setProgress(66, "loading tree panel");
				
				tabs.add("Trees", treePanel);
				
				if(seriesID != -1) {
					treeAnalysisPanel = MCMCTreeFunctions.createTreeAnalysisPanel(datacontainer, seriesID);
					tabs.add("Tree Analysis", treeAnalysisPanel);
				}
			}

			if(runTime == false) 
				window.setDataContainer(1, datacontainer);
			else
				window.setDataContainer(2, datacontainer);
			//Link all tabs that should be linked when opened through menu
			if(mainPanel != null) {
				MCMCNumericFunctions.linkMainToTabs(mainPanel, tabs, window);
				if(tablePanel != null) {
					MCMCNumericFunctions.linkMainToTable(mainPanel, tablePanel);
					MCMCNumericFunctions.linkTableToMain(tablePanel, mainPanel);
				}
			}
			if(treePanel != null) { 
				MCMCTreeFunctions.linkTabsToTrees(tabs, treePanel);
				if(seriesID > -1)
					MCMCTreeFunctions.linkTabsToTreeAnalysis(tabs, treeAnalysisPanel);
				if(mainPanel != null) {
					MCMCTreeFunctions.linkMainToTrees(mainPanel, treePanel);
					MCMCTreeFunctions.linkTreesToMain(treePanel, mainPanel);
					if(seriesID > -1) 
						MCMCTreeFunctions.linkMainToTreeAnalysis(mainPanel, treeAnalysisPanel);
				}
			}
			
			frame.dispose();
			window.setVisible(true);
			window.appear();	//Make tabs appear
			window.windowAppear();
			window.addTab(datacontainer.getFileName(), tabs);	//Name the tab after file
			window.selectTab(window.getTabs().getTabCount()-1);	//Switch to most recent tab
			window.setCursor(null);
		}
	}
	
	public static void parallelFileOpener(File file1, final MCMCWindow window) {
		MCMCDataContainer datacontainer 		= null;
		MCMCDataContainer combinedContainer     = null;
		MCMCParallelMainTab mainPanel 			= null;
		MCMCTableTab tablePanel 				= null;
		MCMCTreeParallelTab treePanel 			= null;
		MCMCTreeAnalysisTab treeAnalysisPanel 	= null;
		JTabbedPane tabs 						= new JTabbedPane();
		String decoded 							= "";
		Object[] serie;
		int seriesID 							= -1;
		int overallConvergenceESS 				= -2;
		int burnin;
		int serieLength;
		int ess;
		
		JFrame frame = new JFrame("VMCMC Parallel Progress");
		frame.setBackground(new Color(0xFFEEEEFF));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        MCMCProgressBar progressBar = new MCMCProgressBar(70);
        JComponent newContentPane = progressBar;
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
        frame.setVisible(true);
        frame.update(frame.getGraphics());
		window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		try {
			//Call library method to store contents of file to MCMCDataContainer
			datacontainer = MCMCFileReader.readMCMCFile(file1, true, false);	
			progressBar.setProgress(70, "Combining runs for parallel traces");
			if(datacontainer.getNumValueSeries() != 0) {
				for (int i = 0; i < datacontainer.getNumValueSeries(); i++) {
					serie = datacontainer.getValueSerie(i).toArray();
					serieLength	= serie.length;

					if(serieLength < 100){
						overallConvergenceESS = -1;
						break;
					}

					ess = MCMCMath.calculateESSmax(serie);
					
					if(ess < 0) {
						overallConvergenceESS = -1;
						break;
					}

					if(overallConvergenceESS != -1 && ess > overallConvergenceESS) 
						overallConvergenceESS = ess;
				}
			} 
			if(overallConvergenceESS == -1) 
				burnin = 0;
			else
				burnin = overallConvergenceESS;
			datacontainer.setBurnin(burnin);
			
			window.setDataContainer(2, datacontainer);
			
			if(window.getDataContainer(1).getNumLines() != window.getDataContainer(2).getNumLines()) {
				JOptionPane.showMessageDialog(new JFrame(), "Different number of samples in both chains. Trace draw utility requires equal number of samples or use command line for unequal samples.");
				return;
			}

			combinedContainer = MCMCNumericFunctions.combineTwoContainers(window.getDataContainer(1), window.getDataContainer(2));

			if(combinedContainer == null) {
				JOptionPane.showMessageDialog(new JFrame(), "At least one parameter of the two chains has not converged. Can not display parallel analysis for a non-converged chain.");
				return;
			}

			tabs.setBackground(new Color(0xFFDDDDFF));
			tabs.setFocusable(false);
			tabs.setBorder(BorderFactory.createLineBorder(new Color(0xFFEEEEFF), 2));

			//If there are numerical values in file. Create main panel and table panel
			if(combinedContainer.getNumValueSeries() != 0) {
				mainPanel = MCMCNumericFunctions.createMainPanelforParallel(combinedContainer, window);
				progressBar.setProgress(80, "Computing parallel graph panel");
				tablePanel = MCMCNumericFunctions.createTablePanel(combinedContainer);
				progressBar.setProgress(90, "Loading parallel table panel");
				
				tabs.add("Graph", mainPanel);
				tabs.add("Table", tablePanel);
				
				//Link all tabs that should be linked when opened through menu
				if(mainPanel != null) {
					if(tablePanel != null)
						MCMCNumericFunctions.linkTableToMaininParallel(tablePanel, mainPanel);
				}
			}
		} catch(IOException e) {
			JOptionPane.showMessageDialog(new JFrame(), "Input Error. Verify that the file exists and the correct path is provided.");
			return;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(), "Invalid MCMC file format. Currently MCMC runs from PrIMe, JPrIMe, BEAST and MrBayes are supported. \nSee examples on https://code.google.com/p/vmcmc/ for further directions.");
			return;
		}
		
		//If there are tree parameters in file. Create tree tab.
		if(combinedContainer.getNumTreeSeries() != 0) {			
			for(int i = 0; i < combinedContainer.getNumTreeSeries(); i++) {
				try {
					decoded = new String((combinedContainer.getTreeSerie(i).get(0).getData()), "UTF-8");
				} catch (UnsupportedEncodingException e) {
				}
				if(decoded.indexOf(":") == -1)
					seriesID = i;
			}
			
			treePanel = MCMCTreeFunctions.createTreeParallelPanel(combinedContainer, window);
						
			tabs.add("Trees", treePanel);
			if(seriesID > -1) {
				treeAnalysisPanel = MCMCTreeFunctions.createTreeAnalysisPanel(combinedContainer, seriesID);
				tabs.add("Tree Analysis", treeAnalysisPanel);
			}
		}

		if(treePanel != null) {
			MCMCTreeFunctions.linkTabsToTreesParallel(tabs, treePanel, window);
			MCMCTreeFunctions.linkMainToTreesParallel(mainPanel, treePanel, window);
			if(seriesID > -1) {
				MCMCTreeFunctions.linkTabsToTreeAnalysisParallel(tabs, treeAnalysisPanel);
				MCMCTreeFunctions.linkMainToTreeAnalysisParallel(mainPanel, treeAnalysisPanel);
			}
		}
		if(mainPanel != null && treePanel != null) {
			MCMCTreeFunctions.linkMainToTreesParallel(mainPanel, treePanel, window);
			if(seriesID > -1) 
				MCMCTreeFunctions.linkMainToTreeAnalysisParallel(mainPanel, treeAnalysisPanel);
		}
		frame.dispose();
		window.setCursor(null);
		window.appear();	//Make tabs appear
		window.windowAppear();
		window.addTab(combinedContainer.getFileName(), tabs);	//Name the tab after file
		window.selectTab(window.getTabs().getTabCount()-1);
	}
}
