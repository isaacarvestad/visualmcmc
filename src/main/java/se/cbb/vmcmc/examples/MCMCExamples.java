package se.cbb.vmcmc.examples;

import java.io.File;

import se.cbb.vmcmc.gui.MCMCWindow;
import se.cbb.vmcmc.libs.SequenceFileWriter;
import se.cbb.vmcmc.main.MCMCFileAnalyser;

public class MCMCExamples {
	public static void mcmcExample(MCMCWindow window, String program) {
		String name = "";
		if(program.equals("PrIMe"))
			name = "/Sample_PrIMe.txt";
		else if(program.equals("JPrIMe"))
			name = "/Sample_JPrIMe.mcmc";
		else if(program.equals("MrBayesStationary"))
			name = "/MrBayes.mcmc";

		MCMCExamplesFunctions.readExampleFiles(window, name, false);
	}

	public static void mcmcLargeDatasets(MCMCWindow window) {
		MCMCExamplesFunctions.readExampleFiles(window, "/Large_JPrime.mcmc", false);
	}

	public static void mcmcParallelExample(MCMCWindow window) {
		MCMCExamplesFunctions.readExampleFiles(window, "/Sample_Run1.mcmc", true);
		MCMCExamplesFunctions.readExampleParallelFiles(window, "/Sample_Run2.mcmc");
	}

	public static void mcmcConvergedParallelExample(MCMCWindow window) {
		MCMCExamplesFunctions.readExampleFiles(window, "/CData1.mcmc", true);
		MCMCExamplesFunctions.readExampleParallelFiles(window, "/CData2.mcmc");
	}

	public static void mrBayesParallelExample(MCMCWindow window) {
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/primates.nex.run1.p");
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/primates.nex.run1.t");
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/primates.nex.run2.p");
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/primates.nex.run2.t");

		File file = new File("primates.nex.run1.p");
		MCMCFileAnalyser.fileOpener(file, window, false, true);

		File file1 = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length()-7) + ".run2.p");
		MCMCFileAnalyser.fileOpener(file1, window, true, true);
		MCMCFileAnalyser.parallelFileOpener(file1, window);

		SequenceFileWriter.FileDeleter("primates.nex.run1.p");
		SequenceFileWriter.FileDeleter("primates.nex.run1.t");
		SequenceFileWriter.FileDeleter("primates.nex.run2.p");
		SequenceFileWriter.FileDeleter("primates.nex.run2.t");
	}

	public static void mrBayesExample(MCMCWindow window) {
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/primates.nex.run1.p");
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/primates.nex.run1.t");

		File file = new File("primates.nex.run1.p");
		MCMCFileAnalyser.fileOpener(file, window, false, false);

		SequenceFileWriter.FileDeleter("primates.nex.run1.p");
		SequenceFileWriter.FileDeleter("primates.nex.run1.t");
	}

	public static void beastExample(MCMCWindow window) {
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/Beast.txt");
		MCMCExamplesFunctions.readExampleBayesFiles(window, "/Beast.t");

		File file = new File("Beast.txt");
		MCMCFileAnalyser.fileOpener(file, window, false, false);

		SequenceFileWriter.FileDeleter("Beast.txt");
		SequenceFileWriter.FileDeleter("Beast.t");
	}
}
