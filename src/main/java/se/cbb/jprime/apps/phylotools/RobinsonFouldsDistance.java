package se.cbb.jprime.apps.phylotools;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterDescription;

import se.cbb.jprime.consensus.day.RobinsonFoulds;
import se.cbb.jprime.io.NewickTree;
import se.cbb.jprime.io.NewickTreeReader;

/**
 * Computes the Robinson-Foulds distance between trees. Two modes are supported:
 * <ol>
 * <li>One input file: Computes a symmetric matrix for the all-vs.-all comparison of the trees in the file.</li>
 * <li>Two input files: Assumes that the files are ordered for per-line comparisons of trees.</li>
 * </ol>
 * 
 * @author Joel Sjöstrand.
 */
public class RobinsonFouldsDistance {
	
	/**
	 * Starter.
	 * @param args.
	 */
	public void main(String[] args) {
		try {
			
			// ================ PARSE USER OPTIONS AND ARGUMENTS ================
			
			RobinsonFouldsDistanceParameters params = new RobinsonFouldsDistanceParameters();
			JCommander jc = new JCommander(params, args);
			if (args.length == 0 || params.help) {
				StringBuilder sb = new StringBuilder(65536);
				sb.append(
						"================================================================================\n" +
						" Computes the symmetric Robinson-Foulds distance between trees. Two modes are\n" +
						" supported:\n" +
						" 1) One input file: Computes a matrix of all-vs.-all tree comparisons for the\n" +
						"    trees in the file. Output is a tab-delimited symmetric matrix.\n" +
						" 2) Two input files: Assumes that the files are ordered for paired comparisons\n" +
						"    of trees. Output is a list with the same number of lines.\n" +
						" Trees must be provided on the Newick format.\n" +
						"================================================================================\n");
				sb.append("Usage:\n" +
						"    java -cp jprime-X.Y.Z.jar se/cbb/jprime/apps/phylotools/RobinsonFouldsDistance [options] <infile> [infile2]\n");
				getUnsortedUsage(jc, params, sb);
				System.out.println(sb.toString());
				return;
			}	
			
			
			// ================ COMPUTE DISTANCES ================
			
			if (params.infiles.size() == 1) {
				// Create matrix of comparisons.
				File f = new File(params.infiles.get(0));
				List<NewickTree> trees = NewickTreeReader.readTrees(f, false);
				double[][] dists = RobinsonFoulds.computeDistanceMatrix(trees, params.unrooted);
				for (int i = 0; i < trees.size(); ++i) {
					for (int j = 0; j < trees.size(); ++j) {
						System.out.print("" + ((int)(dists[i][j])) + '\t');
					}
					System.out.print("\n");
				}
			} else if (params.infiles.size() == 2) {
				// Create pairwise comparisons.
				File f1 = new File(params.infiles.get(0));
				File f2 = new File(params.infiles.get(1));
				List<NewickTree> trees1 = NewickTreeReader.readTrees(f1, false);
				List<NewickTree> trees2 = NewickTreeReader.readTrees(f2, false);
				double[] dists = RobinsonFoulds.computePairedDistances(trees1, trees2, params.unrooted);
				for (double dist : dists) {
					System.out.println((int)dist);
				}
			} else {
				throw new IllegalArgumentException("Must have one or two input files.");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.err.print("\nUse option -h or --help to show usage.\n");
		}
	}

	/**
	 * Obtains JCommander arguments in order of appearance rather than sorted
	 * by name (which is the case with a plain usage() call in JCommander 1.18).
	 * @param jc JCommander object.
	 * @param parameters class with JCommander parameters (and nothing else!).
	 * @param out string builder to write to.
	 * @return the usage.
	 */
	public static void getUnsortedUsage(JCommander jc, Object jcParams, StringBuilder out) {
		// Special treatment of main parameter.
		ParameterDescription mainParam = jc.getMainParameter();
		if (mainParam != null) {
			out.append("Required arguments:\n");
			out.append("     ").append(mainParam.getDescription()).append('\n');
		}
		out.append("Options:\n");
		List<ParameterDescription> params = jc.getParameters();
		Field[] fields = jcParams.getClass().getFields();
		for (Field f : fields) {
			for (ParameterDescription p : params) {
				if (f.getName().equals(p.getField().getName())) {
					out.append(p.getNames()).append('\n');
					String def = (p.getDefault() == null ? "" : " Default: " + p.getDefault().toString() + '.');
					String desc = WordUtils.wrap(p.getDescription() + def, 75);
					desc = "     " + desc;
					desc = desc.replaceAll("\n", "\n     ") + '\n';
					out.append(desc);
					break;
				}
			}
		}
	}
}
